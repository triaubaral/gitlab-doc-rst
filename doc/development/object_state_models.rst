Object state models
===================

Diagrams
--------

`GitLab object state
models <https://drive.google.com/drive/u/3/folders/0B5tDlHAM4iZINmpvYlJXcDVqMGc>`__

--------------

Legend
------

.. figure:: img/state-model-legend.png
   :alt: legend

   legend

--------------

Issue
-----

```app/models/issue.rb`` <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/app/models/issue.rb>`__
|issue|

--------------

Merge request
-------------

```app/models/merge_request.rb`` <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/app/models/merge_request.rb>`__
|merge request|

.. |issue| image:: img/state-model-issue.png
.. |merge request| image:: img/state-model-merge-request.png

