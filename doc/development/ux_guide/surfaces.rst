Surfaces
========

Contents
--------

-  `Header <#header>`__
-  `Global menu <#global-menu>`__
-  `Side pane <#side-pane>`__
-  `Content area <#content-area>`__

--------------

.. figure:: img/surfaces-ux.png
   :alt: Surfaces UX

   Surfaces UX

Global menu
-----------

This menu is to navigate to pages that contain content global to GitLab.

--------------

Header
------

The header contains 3 main elements: Project switching and searching,
user account avatar and settings, and a contextual menu that changes
based on the current page.

.. figure:: img/surfaces-header.png
   :alt: Surfaces Header

   Surfaces Header

--------------

Side pane
---------

The side pane holds supporting information and meta data for the
information in the content area.

--------------

Content area
------------

The main content of the page. The content area can include other
surfaces.

Item title bar
~~~~~~~~~~~~~~

The item title bar contains the top level information to identify the
item, such as the name, id and status.

.. figure:: img/surfaces-contentitemtitle.png
   :alt: Item title

   Item title

Item system information
~~~~~~~~~~~~~~~~~~~~~~~

The system information block contains relevant system controlled
information.

.. figure:: img/surfaces-systeminformationblock.png
   :alt: Item system information

   Item system information
