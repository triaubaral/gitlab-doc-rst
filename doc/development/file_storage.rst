File Storage in GitLab
======================

We use the
`CarrierWave <https://github.com/carrierwaveuploader/carrierwave>`__ gem
to handle file upload, store and retrieval.

There are many places where file uploading is used, according to
contexts:

-  System
-  Instance Logo (logo visible in sign in/sign up pages)
-  Header Logo (one displayed in the navigation bar)
-  Group
-  Group avatars
-  User
-  User avatars
-  User snippet attachments
-  Project
-  Project avatars
-  Issues/MR Markdown attachments
-  Issues/MR Legacy Markdown attachments
-  CI Build Artifacts
-  LFS Objects

Disk storage
------------

GitLab started saving everything on local disk. While directory location
changed from previous versions, they are still not 100% standardized.
You can see them below:

+---------------------+-----+---------------------------------+-------------+-------+
| Description         | In  | Relative path                   | Uploader    | model |
|                     | DB? |                                 | class       | \_typ |
|                     |     |                                 |             | e     |
+=====================+=====+=================================+=============+=======+
| Instance logo       | yes | uploads/-/system/appearance/log | ``Attachmen | Appea |
|                     |     | o/:id/:filename                 | tUploader`` | rance |
+---------------------+-----+---------------------------------+-------------+-------+
| Header logo         | yes | uploads/-/system/appearance/hea | ``Attachmen | Appea |
|                     |     | der\_logo/:id/:filename         | tUploader`` | rance |
+---------------------+-----+---------------------------------+-------------+-------+
| Group avatars       | yes | uploads/-/system/group/avatar/: | ``AvatarUpl | Group |
|                     |     | id/:filename                    | oader``     |       |
+---------------------+-----+---------------------------------+-------------+-------+
| User avatars        | yes | uploads/-/system/user/avatar/:i | ``AvatarUpl | User  |
|                     |     | d/:filename                     | oader``     |       |
+---------------------+-----+---------------------------------+-------------+-------+
| User snippet        | yes | uploads/-/system/personal\_snip | ``PersonalF | Snipp |
| attachments         |     | pet/:id/:random\_hex/:filename  | ileUploader | et    |
|                     |     |                                 | ``          |       |
+---------------------+-----+---------------------------------+-------------+-------+
| Project avatars     | yes | uploads/-/system/project/avatar | ``AvatarUpl | Proje |
|                     |     | /:id/:filename                  | oader``     | ct    |
+---------------------+-----+---------------------------------+-------------+-------+
| Issues/MR Markdown  | yes | uploads/:project\_path\_with\_n | ``FileUploa | Proje |
| attachments         |     | amespace/:random\_hex/:filename | der``       | ct    |
+---------------------+-----+---------------------------------+-------------+-------+
| Issues/MR Legacy    | no  | uploads/-/system/note/attachmen | ``Attachmen | Note  |
| Markdown            |     | t/:id/:filename                 | tUploader`` |       |
| attachments         |     |                                 |             |       |
+---------------------+-----+---------------------------------+-------------+-------+
| CI Artifacts (CE)   | yes | shared/artifacts/:year\_:month/ | ``ArtifactU | Ci::B |
|                     |     | :project\_id/:id                | ploader``   | uild  |
+---------------------+-----+---------------------------------+-------------+-------+
| LFS Objects (CE)    | yes | shared/lfs-objects/:hex/:hex/:o | ``LfsObject | LfsOb |
|                     |     | bject\_hash                     | Uploader``  | ject  |
+---------------------+-----+---------------------------------+-------------+-------+

CI Artifacts and LFS Objects behave differently in CE and EE. In CE they
inherit the ``GitlabUploader`` while in EE they inherit the
``ObjectStoreUploader`` and store files in and S3 API compatible object
store.

In the case of Issues/MR Markdown attachments, there is a different
approach using the `Hashed
Storage <../administration/repository_storage_types.md>`__ layout,
instead of basing the path into a mutable variable
``:project_path_with_namespace``, it's possible to use the hash of the
project ID instead, if project migrates to the new approach (introduced
in 10.2).
