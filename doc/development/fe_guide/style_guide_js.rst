Style guides and linting
========================

See the relevant style guides for our guidelines and for information on
linting:

JavaScript
----------

We defer to `Airbnb <https://github.com/airbnb/javascript>`__ on most
style-related conventions and enforce them with eslint.

See `our current
.eslintrc <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.eslintrc>`__
for specific rules and patterns.

Common
~~~~~~

ESlint
^^^^^^

1.  **Never** disable eslint rules unless you have a good reason. You
    may see a lot of legacy files with
    ``/* eslint-disable some-rule, some-other-rule */`` at the top, but
    legacy files are a special case. Any time you develop a new feature
    or refactor an existing one, you should abide by the eslint rules.

2.  **Never Ever EVER** disable eslint globally for a file
    \`\`\`javascript // bad /\* eslint-disable \*/

    // better /\* eslint-disable some-rule, some-other-rule \*/

    // best // nothing :) \`\`\`

3.  If you do need to disable a rule for a single violation, try to do
    it as locally as possible \`\`\`javascript // bad /\* eslint-disable
    no-new \*/

    import Foo from 'foo';

    new Foo();

    // better import Foo from 'foo';

    // eslint-disable-next-line no-new new Foo(); \`\`\`
4.  There are few rules that we need to disable due to technical debt.
    Which are:
5.  `no-new <http://eslint.org/docs/rules/no-new>`__
6.  `class-methods-use-this <http://eslint.org/docs/rules/class-methods-use-this>`__

7.  When they are needed *always* place ESlint directive comment blocks
    on the first line of a script, followed by any global declarations,
    then a blank newline prior to any imports or code. \`\`\`javascript
    // bad /\* global Foo */ /* eslint-disable no-new \*/ import Bar
    from './bar';

    // good /\* eslint-disable no-new */ /* global Foo \*/

    import Bar from './bar'; \`\`\`

8.  **Never** disable the ``no-undef`` rule. Declare globals with
    ``/* global Foo */`` instead.

9.  When declaring multiple globals, always use one
    ``/* global [name] */`` line per variable. \`\`\`javascript // bad
    /\* globals Flash, Cookies, jQuery \*/

    // good /\* global Flash */ /* global Cookies */ /* global jQuery
    \*/ \`\`\`

10. Use up to 3 parameters for a function or class. If you need more
    accept an Object instead. \`\`\`javascript // bad fn(p1, p2, p3, p4)
    {}

    // good fn(options) {} \`\`\`

Modules, Imports, and Exports
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Use ES module syntax to import modules

   .. code:: javascript

         // bad
         const SomeClass = require('some_class');

         // good
         import SomeClass from 'some_class';

         // bad
         module.exports = SomeClass;

         // good
         export default SomeClass;

   Import statements are following usual naming guidelines, for example
   object literals use camel case:

   .. code:: javascript

         // some_object file
         export default {
           key: 'value',
         };

         // bad
         import ObjectLiteral from 'some_object';

         // good
         import objectLiteral from 'some_object';

2. Relative paths: when importing a module in the same directory, a
   child directory, or an immediate parent directory prefer relative
   paths. When importing a module which is two or more levels up, prefer
   either ``~/`` or ``ee/``.

   In **app/assets/javascripts/my-feature/subdir**:

   .. code:: javascript

       // bad
       import Foo from '~/my-feature/foo';
       import Bar from '~/my-feature/subdir/bar';
       import Bin from '~/my-feature/subdir/lib/bin';

       // good
       import Foo from '../foo';
       import Bar from './bar';
       import Bin from './lib/bin';

   In **spec/javascripts**:

   .. code:: javascript

       // bad
       import Foo from '../../app/assets/javascripts/my-feature/foo';

       // good
       import Foo from '~/my-feature/foo';

   When referencing an **EE component**:

   .. code:: javascript

       // bad
       import Foo from '../../../../../ee/app/assets/javascripts/my-feature/ee-foo';

       // good
       import Foo from 'ee/my-feature/foo';

3. Avoid using IIFE. Although we have a lot of examples of files which
   wrap their contents in IIFEs (immediately-invoked function
   expressions), this is no longer necessary after the transition from
   Sprockets to webpack. Do not use them anymore and feel free to remove
   them when refactoring legacy code.

4. Avoid adding to the global namespace.

   .. code:: javascript

         // bad
         window.MyClass = class { /* ... */ };

         // good
         export default class MyClass { /* ... */ }

5. Side effects are forbidden in any script which contains exports

   .. code:: javascript

         // bad
         export default class MyClass { /* ... */ }

         document.addEventListener("DOMContentLoaded", function(event) {
           new MyClass();
         }

Data Mutation and Pure functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Strive to write many small pure functions, and minimize where
   mutations occur. \`\`\`javascript // bad const values = {foo: 1};

   function impureFunction(items) { const bar = 1;

   items.foo = items.a \* bar + 2;

   return items.a; }

   const c = impureFunction(values);

   // good var values = {foo: 1};

   function pureFunction (foo) { var bar = 1;

   foo = foo \* bar + 2;

   return foo; }

   var c = pureFunction(values.foo); \`\`\`

2. Avoid constructors with side-effects

3. Prefer ``.map``, ``.reduce`` or ``.filter`` over ``.forEach`` A
   forEach will cause side effects, it will be mutating the array being
   iterated. Prefer using ``.map``, ``.reduce`` or ``.filter``
   \`\`\`javascript const users = [ { name: 'Foo' }, { name: 'Bar' } ];

   // bad users.forEach((user, index) => { user.id = index; });

   // good const usersWithId = users.map((user, index) => { return
   Object.assign({}, user, { id: index }); }); \`\`\`

Parse Strings into Numbers
^^^^^^^^^^^^^^^^^^^^^^^^^^

1. ``parseInt()`` is preferable over ``Number()`` or ``+``
   \`\`\`javascript // bad +'10' // 10

   // good Number('10') // 10

   // better parseInt('10', 10); \`\`\`

CSS classes used for JavaScript
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. If the class is being used in Javascript it needs to be prepend with
   ``js-`` \`\`\`html // bad

   .. raw:: html

      <button class="add-user">

   Add User

   .. raw:: html

      </button>

   // good

   .. raw:: html

      <button class="js-add-user">

   Add User

   .. raw:: html

      </button>

   \`\`\`

Vue.js
~~~~~~

Basic Rules
^^^^^^^^^^^

1. The service has it's own file
2. The store has it's own file
3. Use a function in the bundle file to instantiate the Vue component:
   \`\`\`javascript // bad class { init() { new Component({}) } }

   // good document.addEventListener('DOMContentLoaded', () => new Vue({
   el: '#element', components: { componentName }, render: createElement
   => createElement('component-name'), })); \`\`\`

4. Don not use a singleton for the service or the store \`\`\`javascript
   // bad class Store { constructor() { if (!this.prototype.singleton) {
   // do something } } }

   // good class Store { constructor() { // do something } } \`\`\`

Naming
^^^^^^

1. **Extensions**: Use ``.vue`` extension for Vue components.
2. **Reference Naming**: Use camelCase for their instances:
   \`\`\`javascript // bad import CardBoard from 'cardBoard'

   components: { CardBoard: };

   // good import cardBoard from 'cardBoard'

   components: { cardBoard: }; \`\`\`

3. **Props Naming:** Avoid using DOM component prop names.
4. **Props Naming:** Use kebab-case instead of camelCase to provide
   props in templates. \`\`\`javascript // bad

   // good

   // bad

   // good \`\`\`

Alignment
^^^^^^^^^

1. Follow these alignment styles for the template method:
2. With more than one attribute, all attributes should be on a new line:
   \`\`\`javascript // bad

   ::

       <button class="btn">Click me</button>

       // good
       <component
         v-if="bar"
         param="baz"
       />

       <button class="btn">
         Click me
       </button>

   \`\`\`
3. The tag can be inline if there is only one attribute:
   \`\`\`javascript // good

   ::

       // good
         <component
           bar="bar"
           />

   \`\`\`

Quotes
^^^^^^

1. Always use double quotes ``"`` inside templates and single quotes
   ``'`` for all other JS. \`\`\ ``javascript // bad template:``

   .. raw:: html

      <button :class="style">

   Button

   .. raw:: html

      </button>

   \`

   // good template: ``<button :class="style">Button</button>`` \`\`\`

Props
^^^^^

1. Props should be declared as an object \`\`\`javascript // bad props:
   ['foo']

   // good props: { foo: { type: String, required: false, default: 'bar'
   } } \`\`\`

2. Required key should always be provided when declaring a prop
   \`\`\`javascript // bad props: { foo: { type: String, } }

   // good props: { foo: { type: String, required: false, default: 'bar'
   } } \`\`\`

3. Default key should be provided if the prop is not required. *Note:*
   There are some scenarios where we need to check for the existence of
   the property. On those a default key should not be provided.
   \`\`\`javascript // good props: { foo: { type: String, required:
   false, } }

   // good props: { foo: { type: String, required: false, default: 'bar'
   } }

   // good props: { foo: { type: String, required: true } } \`\`\`

Data
^^^^

1. ``data`` method should always be a function

   .. code:: javascript

         // bad
         data: {
           foo: 'foo'
         }

         // good
         data() {
           return {
             foo: 'foo'
           };
         }

Directives
^^^^^^^^^^

1. Shorthand ``@`` is preferable over ``v-on`` \`\`\`javascript // bad

   // good \`\`\`

2. Shorthand ``:`` is preferable over ``v-bind`` \`\`\`javascript // bad

   // good \`\`\`

Closing tags
^^^^^^^^^^^^

1. Prefer self closing component tags \`\`\`javascript // bad

   // good \`\`\`

Ordering
^^^^^^^^

1.  Tag order in ``.vue`` file

    ::

        <script>
          // ...
        </script>

        <template>
          // ...
        </template>

        // We don't use scoped styles but there are few instances of this
        <style>
          // ...
        </style>

2.  Properties in a Vue Component:
3.  ``name``
4.  ``props``
5.  ``mixins``
6.  ``directives``
7.  ``data``
8.  ``components``
9.  ``computedProps``
10. ``methods``
11. ``beforeCreate``
12. ``created``
13. ``beforeMount``
14. ``mounted``
15. ``beforeUpdate``
16. ``updated``
17. ``activated``
18. ``deactivated``
19. ``beforeDestroy``
20. ``destroyed``

Vue and Bootstrap
^^^^^^^^^^^^^^^^^

1. Tooltips: Do not rely on ``has-tooltip`` class name for Vue
   components \`\`\`javascript // bad Text

   // good Text \`\`\`

2. Tooltips: When using a tooltip, include the tooltip directive,
   ``./app/assets/javascripts/vue_shared/directives/tooltip.js``

3. Don't change ``data-original-title``. \`\`\`javascript // bad Foo

   // good Foo

   $('span').tooltip('fixTitle'); \`\`\`

The Javascript/Vue Accord
~~~~~~~~~~~~~~~~~~~~~~~~~

The goal of this accord is to make sure we are all on the same page.

1.  When writing Vue, you may not use jQuery in your application.
2.  If you need to grab data from the DOM, you may query the DOM 1 time
    while bootstrapping your application to grab data attributes using
    ``dataset``. You can do this without jQuery.
3.  You may use a jQuery dependency in Vue.js following `this example
    from the docs <https://vuejs.org/v2/examples/select2.html>`__.
4.  If an outside jQuery Event needs to be listen to inside the Vue
    application, you may use jQuery event listeners.
5.  We will avoid adding new jQuery events when they are not required.
    Instead of adding new jQuery events take a look at `different
    methods to do the same task <https://vuejs.org/v2/api/#vm-emit>`__.
6.  You may query the ``window`` object 1 time, while bootstrapping your
    application for application specific data (e.g. ``scrollTo`` is ok
    to access anytime). Do this access during the bootstrapping of your
    application.
7.  You may have a temporary but immediate need to create technical debt
    by writing code that does not follow our standards, to be refactored
    later. Maintainers need to be ok with the tech debt in the first
    place. An issue should be created for that tech debt to evaluate it
    further and discuss. In the coming months you should fix that tech
    debt, with it's priority to be determined by maintainers.
8.  When creating tech debt you must write the tests for that code
    before hand and those tests may not be rewritten. e.g. jQuery tests
    rewritten to Vue tests.
9.  You may choose to use VueX as a centralized state management. If you
    choose not to use VueX, you must use the *store pattern* which can
    be found in the `Vue.js
    documentation <https://vuejs.org/v2/guide/state-management.html#Simple-State-Management-from-Scratch>`__.
10. Once you have chosen a centralized state management solution you
    must use it for your entire application. i.e. Don't mix and match
    your state management solutions.

SCSS
----

-  `SCSS <style_guide_scss.md>`__
