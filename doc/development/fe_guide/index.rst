Frontend Development Guidelines
===============================

This document describes various guidelines to ensure consistency and
quality across GitLab's frontend team.

Overview
--------

GitLab is built on top of `Ruby on Rails <http://rubyonrails.org/>`__
using `Haml <http://haml.info/>`__ with
`Hamlit <https://github.com/k0kubun/hamlit>`__. Be wary of `the
limitations that come with using
Hamlit <https://github.com/k0kubun/hamlit/blob/master/REFERENCE.md#limitations>`__.
We also use `SCSS <http://sass-lang.com/>`__ and plain JavaScript with
modern ECMAScript standards supported through
`Babel <https://babeljs.io/>`__ and ES module support through
`webpack <https://webpack.js.org/>`__.

We also utilize `webpack <https://webpack.js.org/>`__ to handle the
bundling, minification, and compression of our assets.

Working with our frontend assets requires Node (v4.3 or greater) and
Yarn (v0.17 or greater). You can find information on how to install
these on our `installation
guide <../../install/installation.md#4-node>`__.

`jQuery <https://jquery.com/>`__ is used throughout the application's
JavaScript, with `Vue.js <http://vuejs.org/>`__ for particularly
advanced, dynamic elements.

Browser Support
~~~~~~~~~~~~~~~

For our currently-supported browsers, see our
`requirements <../../install/requirements.md#supported-web-browsers>`__.

--------------

Development Process
-------------------

Share your work early
~~~~~~~~~~~~~~~~~~~~~

1. Before writing code guarantee your vision of the architecture is
   aligned with GitLab's architecture.
2. Add a diagram to the issue and ask a Frontend Architecture about it.

.. figure:: img/boards_diagram.png
   :alt: Diagram of Issue Boards Architecture

   Diagram of Issue Boards Architecture

1. Don't take more than one week between starting work on a feature and
   sharing a Merge Request with a reviewer or a maintainer.

Vue features
~~~~~~~~~~~~

1. Follow the steps in `Vue.js Best Practices <vue.md>`__
2. Follow the style guide.
3. Only a handful of people are allowed to merge Vue related features.
   Reach out to one of Vue experts early in this process.

--------------

`Architecture <architecture.md>`__
----------------------------------

How we go about making fundamental design decisions in GitLab's frontend
team or make changes to our frontend development guidelines.

--------------

`Testing <../testing_guide/frontend_testing.md>`__
--------------------------------------------------

How we write frontend tests, run the GitLab test suite, and debug test
related issues.

--------------

`Design Patterns <design_patterns.md>`__
----------------------------------------

Common JavaScript design patterns in GitLab's codebase.

--------------

`Vue.js Best Practices <vue.md>`__
----------------------------------

Vue specific design patterns and practices.

--------------

`Axios <axios.md>`__
--------------------

Axios specific practices and gotchas.

`Icons <icons.md>`__
--------------------

How we use SVG for our Icons.

`Dropdowns <dropdowns.md>`__
----------------------------

How we use dropdowns.
---------------------

Style Guides
------------

`JavaScript Style Guide <style_guide_js.md>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We use eslint to enforce our JavaScript style guides. Our guide is based
on the excellent `Airbnb <https://github.com/airbnb/javascript>`__ style
guide with a few small changes.

`SCSS Style Guide <style_guide_scss.md>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Our SCSS conventions which are enforced through
`scss-lint <https://github.com/brigade/scss-lint>`__.

--------------

`Performance <performance.md>`__
--------------------------------

Best practices for monitoring and maximizing frontend performance.

--------------

`Security <security.md>`__
--------------------------

Frontend security practices.

--------------

`Accessibility <accessibility.md>`__
------------------------------------

Our accessibility standards and resources.

`Internationalization (i18n) and Translations <../i18n/externalization.md>`__
-----------------------------------------------------------------------------

Frontend internationalization support is described in `this
document <../i18n/>`__. The `externalization part of the
guide <../i18n/externalization.md>`__ explains the helpers/methods
available.

--------------

`DropLab <droplab/droplab.md>`__
--------------------------------

Our internal ``DropLab`` dropdown library.

-  `DropLab <droplab/droplab.md>`__
-  `Ajax plugin <droplab/plugins/ajax.md>`__
-  `Filter plugin <droplab/plugins/filter.md>`__
-  `InputSetter plugin <droplab/plugins/input_setter.md>`__
