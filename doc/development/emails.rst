Dealing with email in development
=================================

Sent emails
-----------

To view rendered emails "sent" in your development instance, visit
```/rails/letter_opener`` <http://localhost:3000/rails/letter_opener>`__.

Mailer previews
---------------

Rails provides a way to preview our mailer templates in HTML and
plaintext using dummy data.

The previews live in
```spec/mailers/previews`` <https://gitlab.com/gitlab-org/gitlab-ce/tree/master/spec/mailers/previews>`__
and can be viewed at
```/rails/mailers`` <http://localhost:3000/rails/mailers>`__.

See the `Rails
guides <http://guides.rubyonrails.org/action_mailer_basics.html#previewing-emails>`__
for more info.

--------------

`Return to Development documentation <README.md>`__
