Testing standards and style guidelines
======================================

This document describes various guidelines and best practices for
automated testing of the GitLab project.

It is meant to be an *extension* of the `thoughtbot testing
styleguide <https://github.com/thoughtbot/guides/tree/master/style/testing>`__.
If this guide defines a rule that contradicts the thoughtbot guide, this
guide takes precedence. Some guidelines may be repeated verbatim to
stress their importance.

Overview
--------

GitLab is built on top of `Ruby on Rails <http://rubyonrails.org/>`__,
and we're using
`RSpec <https://github.com/rspec/rspec-rails#feature-specs>`__ for all
the backend tests, with
`Capybara <https://github.com/teamcapybara/capybara>`__ for end-to-end
integration testing. On the frontend side, we're using
`Karma <http://karma-runner.github.io/>`__ and
`Jasmine <https://jasmine.github.io/>`__ for JavaScript unit and
integration testing.

Following are two great articles that everyone should read to understand
what automated testing means, and what are its principles:

-  `Five Factor
   Testing <https://www.devmynd.com/blog/five-factor-testing>`__: Why do
   we need tests?
-  `Principles of Automated
   Testing <http://www.lihaoyi.com/post/PrinciplesofAutomatedTesting.html>`__:
   Levels of testing. Prioritize tests. Cost of tests.

--------------

`Testing levels <testing_levels.md>`__
--------------------------------------

Learn about the different testing levels, and how to decide at what
level your changes should be tested.

--------------

`Testing best practices <best_practices.md>`__
----------------------------------------------

Everything you should know about how to write good tests: RSpec,
FactoryBot, system tests, parameterized tests etc.

--------------

`Frontend testing standards and style guidelines <frontend_testing.md>`__
-------------------------------------------------------------------------

Everything you should know about how to write good Frontend tests:
Karma, testing promises, stubbing etc.

--------------

`Flaky tests <flaky_tests.md>`__
--------------------------------

What are flaky tests, the different kind of flaky tests we encountered,
and what we do about them.

--------------

`GitLab tests in the Continuous Integration (CI) context <ci.md>`__
-------------------------------------------------------------------

How GitLab test suite is run in the CI context: setup, caches,
artifacts, parallelization, monitoring.

--------------

`Testing Rake tasks <testing_rake_tasks.md>`__
----------------------------------------------

Everything you should know about how to test Rake tasks.

--------------

Spinach (feature) tests
-----------------------

GitLab `moved from Cucumber to
Spinach <https://github.com/gitlabhq/gitlabhq/pull/1426>`__ for its
feature/integration tests in September 2012.

As of March 2016, we are `trying to avoid adding new Spinach
tests <https://gitlab.com/gitlab-org/gitlab-ce/issues/14121>`__ going
forward, opting for `RSpec feature <#features-integration>`__ specs.

Adding new Spinach scenarios is acceptable *only if* the new scenario
requires no more than one new ``step`` definition. If more than that is
required, the test should be re-implemented using RSpec instead.

--------------

`Return to Development documentation <../README.md>`__
