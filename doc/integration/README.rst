GitLab Integration
==================

GitLab integrates with multiple third-party services to allow external
issue trackers and external authentication.

See the documentation below for details on how to configure these
services.

-  `Akismet <akismet.md>`__ Configure Akismet to stop spam
-  `Auth0 OmniAuth <auth0.md>`__ Enable the Auth0 OmniAuth provider
-  `Bitbucket <bitbucket.md>`__ Import projects from Bitbucket.org and
   login to your GitLab instance with your Bitbucket.org account
-  `CAS <cas.md>`__ Configure GitLab to sign in using CAS
-  `External issue tracker <external-issue-tracker.md>`__ Redmine, JIRA,
   etc.
-  `Gmail actions buttons <gmail_action_buttons_for_gitlab.md>`__ Adds
   GitLab actions to messages
-  `JIRA <../user/project/integrations/jira.md>`__ Integrate with the
   JIRA issue tracker
-  `LDAP <ldap.md>`__ Set up sign in via LDAP
-  `OAuth2 provider <oauth_provider.md>`__ OAuth2 application creation
-  `OmniAuth <omniauth.md>`__ Sign in via Twitter, GitHub, GitLab.com,
   Google, Bitbucket, Facebook, Shibboleth, SAML, Crowd, Azure and
   Authentiq ID
-  `OpenID Connect <openid_connect_provider.md>`__ Use GitLab as an
   identity provider
-  `PlantUML <../administration/integration/plantuml.md>`__ Configure
   PlantUML to use diagrams in AsciiDoc documents.
-  `reCAPTCHA <recaptcha.md>`__ Configure GitLab to use Google reCAPTCHA
   for new users
-  `SAML <saml.md>`__ Configure GitLab as a SAML 2.0 Service Provider
-  `Trello <trello_power_up.md>`__ Integrate Trello with GitLab

    GitLab Enterprise Edition contains `advanced Jenkins
    support <http://docs.gitlab.com/ee/integration/jenkins.html>`__.

Project services
----------------

Integration with services such as Campfire, Flowdock, Gemnasium,
HipChat, Pivotal Tracker, and Slack are available in the form of a
`Project Service <../user/project/integrations/project_services.md>`__.

SSL certificate errors
----------------------

When trying to integrate GitLab with services that are using self-signed
certificates, it is very likely that SSL certificate errors will occur
on different parts of the application, most likely Sidekiq. There are 2
approaches you can take to solve this:

1. Add the root certificate to the trusted chain of the OS.
2. If using Omnibus, you can add the certificate to GitLab's trusted
   certificates.

**OS main trusted chain**

This
`resource <http://kb.kerio.com/product/kerio-connect/server-configuration/ssl-certificates/adding-trusted-root-certificates-to-the-server-1605.html>`__
has all the information you need to add a certificate to the main
trusted chain.

This
`answer <http://superuser.com/questions/437330/how-do-you-add-a-certificate-authority-ca-to-ubuntu>`__
at Super User also has relevant information.

**Omnibus Trusted Chain**

`Install the self signed certificate or custom certificate
authorities <http://docs.gitlab.com/omnibus/common_installation_problems/README.html#using-self-signed-certificate-or-custom-certificate-authorities>`__
in to GitLab Omnibus.

It is enough to concatenate the certificate to the main trusted
certificate however it may be overwritten during upgrades:

.. code:: bash

    cat jira.pem >> /opt/gitlab/embedded/ssl/certs/cacert.pem

After that restart GitLab with:

.. code:: bash

    sudo gitlab-ctl restart
