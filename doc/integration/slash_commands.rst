Slash Commands
==============

Slash commands in Mattermost and Slack allow you to control GitLab and
view GitLab content right inside your chat client, without having to
leave it. For Slack, this requires a `project service
configuration <../user/project/integrations/slack_slash_commands.md>`__.
Simply type the command as a message in your chat client to activate it.

Commands are scoped to a project, with a trigger term that is specified
during configuration.

We suggest you use the project name as the trigger term for simplicity
and clarity.

Taking the trigger term as ``project-name``, the commands are:

+----------+---------+
| Command  | Effect  |
+==========+=========+
| ``/proje | Shows   |
| ct-name  | all     |
| help``   | availab |
|          | le      |
|          | slash   |
|          | command |
|          | s       |
+----------+---------+
| ``/proje | Creates |
| ct-name  | a new   |
| issue ne | issue   |
| w <title | with    |
| > <shift | title   |
| +return> | ``<titl |
|  <descri | e>``    |
| ption>`` | and     |
|          | descrip |
|          | tion    |
|          | ``<desc |
|          | ription |
|          | >``     |
+----------+---------+
| ``/proje | Shows   |
| ct-name  | the     |
| issue sh | issue   |
| ow <id>` | with id |
| `        | ``<id>` |
|          | `       |
+----------+---------+
| ``/proje | Shows   |
| ct-name  | up to 5 |
| issue se | issues  |
| arch <qu | matchin |
| ery>``   | g       |
|          | ``<quer |
|          | y>``    |
+----------+---------+
| ``/proje | Deploy  |
| ct-name  | from    |
| deploy < | the     |
| from> to | ``<from |
|  <to>``  | >``     |
|          | environ |
|          | ment    |
|          | to the  |
|          | ``<to>` |
|          | `       |
|          | environ |
|          | ment    |
+----------+---------+

Note that if you are using the `GitLab Slack
application <https://docs.gitlab.com/ee/user/project/integrations/gitlab_slack_application.html>`__
for your GitLab.com projects, you need to `add the ``gitlab`` keyword at
the beginning of the
command <https://docs.gitlab.com/ee/user/project/integrations/gitlab_slack_application.html#usage>`__.

Issue commands
--------------

It is possible to create new issue, display issue details and search up
to 5 issues.

Deploy command
--------------

In order to deploy to an environment, GitLab will try to find a
deployment manual action in the pipeline.

If there is only one action for a given environment, it is going to be
triggered. If there is more than one action defined, GitLab will try to
find an action which name equals the environment name we want to deploy
to.

Command will return an error when no matching action has been found.
