Workflow
========

-  `Automatic issue
   closing <../user/project/issues/automatic_issue_closing.md>`__
-  `Change your time zone <timezone.md>`__
-  `Cycle Analytics <../user/project/cycle_analytics.md>`__
-  `Description templates <../user/project/description_templates.md>`__
-  `Feature branch workflow <workflow.md>`__
-  `GitLab Flow <gitlab_flow.md>`__
-  `Groups <../user/group/index.md>`__
-  Issues - The GitLab Issue Tracker is an advanced and complete tool
   for tracking the evolution of a new idea or the process of solving a
   problem.
-  `Confidential
   issues <../user/project/issues/confidential_issues.md>`__
-  `Due date for issues <../user/project/issues/due_dates.md>`__
-  `Issue Board <../user/project/issue_board.md>`__
-  `Keyboard shortcuts <shortcuts.md>`__
-  `File finder <file_finder.md>`__
-  `Labels <../user/project/labels.md>`__
-  `Notification emails <notifications.md>`__
-  `Projects <../user/project/index.md>`__
-  `Project forking workflow <forking_workflow.md>`__
-  `Project users <../user/project/members/index.md>`__
-  `Protected branches <../user/project/protected_branches.md>`__
-  `Protected tags <../user/project/protected_tags.md>`__
-  `Quick Actions <../user/project/quick_actions.md>`__
-  `Sharing projects with
   groups <../user/project/members/share_project_with_groups.md>`__
-  `Time tracking <time_tracking.md>`__
-  `Web Editor <../user/project/repository/web_editor.md>`__
-  `Releases <releases.md>`__
-  `Milestones <../user/project/milestones/index.md>`__
-  `Merge Requests <../user/project/merge_requests/index.md>`__
-  `Authorization for merge
   requests <../user/project/merge_requests/authorization_for_merge_requests.md>`__
-  `Cherry-pick
   changes <../user/project/merge_requests/cherry_pick_changes.md>`__
-  `Merge when pipeline
   succeeds <../user/project/merge_requests/merge_when_pipeline_succeeds.md>`__
-  `Resolve discussion comments in merge requests
   reviews <../user/discussions/index.md>`__
-  `Resolve merge conflicts in the
   UI <../user/project/merge_requests/resolve_conflicts.md>`__
-  `Revert changes in the
   UI <../user/project/merge_requests/revert_changes.md>`__
-  `Merge requests
   versions <../user/project/merge_requests/versions.md>`__
-  `"Work In Progress" merge
   requests <../user/project/merge_requests/work_in_progress_merge_requests.md>`__
-  `Fast-forward merge
   requests <../user/project/merge_requests/fast_forward_merge.md>`__
-  `Manage large binaries with Git
   LFS <lfs/manage_large_binaries_with_git_lfs.md>`__
-  `Importing from SVN, GitHub, Bitbucket, etc <importing/README.md>`__
-  `Todos <todos.md>`__
-  `Snippets <../user/snippets.md>`__
-  `Subgroups <../user/group/subgroups/index.md>`__
