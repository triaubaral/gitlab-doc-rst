Legal
=====

-  `Corporate contributor license
   agreement <corporate_contributor_license_agreement.md>`__
-  `Individual contributor license
   agreement <individual_contributor_license_agreement.md>`__
