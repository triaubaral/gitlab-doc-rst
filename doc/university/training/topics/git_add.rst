Git Add
=======

--------------

Git Add
-------

Adds content to the index or staging area.

-  Adds a list of file

   .. code:: bash

       git add <files>

-  Adds all files including deleted ones

   .. code:: bash

       git add -A

--------------

Git add continued
-----------------

-  Add all text files in current dir

   .. code:: bash

       git add *.txt

-  Add all text file in the project

   .. code:: bash

       git add "*.txt*"

-  Adds all files in directory

   .. code:: bash

       git add views/layouts/
