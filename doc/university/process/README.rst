Suggesting improvements
=======================

If you would like to teach a class or participate or help in any way
please submit a merge request and assign it to
`Job <https://gitlab.com/u/JobV>`__.

If you have suggestions for additional courses you would like to see,
please submit a merge request to add an upcoming class, assign to
`Chad <https://gitlab.com/u/chadmalchow>`__ and /cc
`Job <https://gitlab.com/u/JobV>`__.

Adding classes
--------------

1. All training materials of any kind should be added to `GitLab
   CE <https://gitlab.com/gitlab-org/gitlab-ce/>`__ to ensure they are
   available to a broad audience (don't use any other repo or storage
   for training materials).
2. Don't make materials that are needlessly specific to one group of
   people, try to keep the wording broad and inclusive (don't make
   things for only GitLab Inc. people, only interns, only customers,
   etc.).
3. To allow people to contribute all content should be in git.
4. The content can go in a subdirectory under ``/doc/university/``.
5. To make, view or modify the slides of the classes use
   `Deckset <http://www.decksetapp.com/>`__ or
   `RevealJS <http://lab.hakim.se/reveal-js/>`__. Do not use Powerpoint
   or Google Slides since this prevents everyone from contributing.
6. Please upload any video recordings to our Youtube channel. We prefer
   them to be public, if needed they can be unlisted but if so they
   should be linked from this page.
7. Please create a merge request and assign to
   `Erica <https://gitlab.com/u/Erica>`__.
