Authentication
==============

This page gathers all the resources for the topic **Authentication**
within GitLab.

GitLab users
------------

-  `SSH <../../ssh/README.md>`__
-  `Two-Factor Authentication
   (2FA) <../../user/profile/account/two_factor_authentication.md#two-factor-authentication>`__
-  `Why do I keep getting signed
   out? <../../user/profile/index.md#why-do-i-keep-getting-signed-out>`__
-  **Articles:**
-  `Support for Universal 2nd Factor Authentication -
   YubiKeys <https://about.gitlab.com/2016/06/22/gitlab-adds-support-for-u2f/>`__
-  `Security Webcast with
   Yubico <https://about.gitlab.com/2016/08/31/gitlab-and-yubico-security-webcast/>`__
-  **Integrations:**
-  `GitLab as OAuth2 authentication service
   provider <../../integration/oauth_provider.md#introduction-to-oauth>`__
-  `GitLab as OpenID Connect identity
   provider <../../integration/openid_connect_provider.md>`__

GitLab administrators
---------------------

-  `LDAP (Community Edition) <../../administration/auth/ldap.md>`__
-  `LDAP (Enterprise
   Edition) <https://docs.gitlab.com/ee/administration/auth/ldap-ee.html>`__
-  `Enforce Two-factor Authentication
   (2FA) <../../security/two_factor_authentication.md#enforce-two-factor-authentication-2fa>`__
-  **Articles:**
-  `How to Configure LDAP with GitLab
   CE <../../administration/auth/how_to_configure_ldap_gitlab_ce/index.md>`__
-  `How to Configure LDAP with GitLab
   EE <https://docs.gitlab.com/ee/articles/how_to_configure_ldap_gitlab_ee/>`__
-  `Feature Highlight: LDAP
   Integration <https://about.gitlab.com/2014/07/10/feature-highlight-ldap-sync/>`__
-  `Debugging
   LDAP <https://about.gitlab.com/handbook/support/workflows/ldap/debugging_ldap.html>`__
-  **Integrations:**
-  `OmniAuth <../../integration/omniauth.md>`__
-  `Authentiq OmniAuth
   Provider <../../administration/auth/authentiq.md#authentiq-omniauth-provider>`__
-  `Atlassian Crowd OmniAuth
   Provider <../../administration/auth/crowd.md>`__
-  `CAS OmniAuth Provider <../../integration/cas.md>`__
-  `SAML OmniAuth Provider <../../integration/saml.md>`__
-  `Okta SSO provider <../../administration/auth/okta.md>`__
-  `Kerberos integration (GitLab
   EE) <https://docs.gitlab.com/ee/integration/kerberos.html>`__

API
---

-  `OAuth 2 Tokens <../../api/README.md#oauth-2-tokens>`__
-  `Private Tokens <../../api/README.md#private-tokens>`__
-  `Impersonation tokens <../../api/README.md#impersonation-tokens>`__
-  `GitLab as an OAuth2
   provider <../../api/oauth2.md#gitlab-as-an-oauth2-provider>`__

Third-party resources
---------------------

-  `Kanboard Plugin GitLab
   Authentication <https://kanboard.net/plugin/gitlab-auth>`__
-  `Jenkins GitLab OAuth
   Plugin <https://wiki.jenkins-ci.org/display/JENKINS/GitLab+OAuth+Plugin>`__
-  `Setup Gitlab CE with Active Directory
   authentication <https://www.caseylabs.com/setup-gitlab-ce-with-active-directory-authentication/>`__
-  `How to customize GitLab to support OpenID
   authentication <http://eric.van-der-vlist.com/blog/2013/11/23/how-to-customize-gitlab-to-support-openid-authentication/>`__
-  `Openshift - Configuring Authentication and User
   Agent <https://docs.openshift.org/latest/install_config/configuring_authentication.html#GitLab>`__
