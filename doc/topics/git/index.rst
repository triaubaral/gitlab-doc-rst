Git documentation
=================

Git is a `free and open
source <https://git-scm.com/about/free-and-open-source>`__ distributed
version control system designed to handle everything from small to very
large projects with speed and efficiency.

`GitLab <https://about.gitlab.com>`__ is a Git-based fully integrated
platform for software development. Besides Git's functionalities, GitLab
has a lot of powerful `features <https://about.gitlab.com/features/>`__
to enhance your
`workflow <https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/>`__.

We've gathered some resources to help you to get the best from Git with
GitLab.

Getting started
---------------

-  `Git
   concepts <../../university/training/user_training.md#git-concepts>`__
-  `Start using Git on the command
   line <../../gitlab-basics/start-using-git.md>`__
-  `Command Line basic
   commands <../../gitlab-basics/command-line-commands.md>`__
-  `GitLab Git Cheat Sheet
   (download) <https://gitlab.com/gitlab-com/marketing/raw/master/design/print/git-cheatsheet/print-pdf/git-cheatsheet.pdf>`__
-  Commits
-  `Revert a
   commit <../../user/project/merge_requests/revert_changes.md#reverting-a-commit>`__
-  `Cherry-picking a
   commit <../../user/project/merge_requests/cherry_pick_changes.md#cherry-picking-a-commit>`__
-  `Squashing
   commits <../../workflow/gitlab_flow.md#squashing-commits-with-rebase>`__
-  **Articles:**
-  `Numerous *undo* possibilities in
   Git <../../articles/numerous_undo_possibilities_in_git/index.md>`__
-  `How to install Git <../../articles/how_to_install_git/index.md>`__
-  `Git Tips &
   Tricks <https://about.gitlab.com/2016/12/08/git-tips-and-tricks/>`__
-  `Eight Tips to help you work better with
   Git <https://about.gitlab.com/2015/02/19/8-tips-to-help-you-work-better-with-git/>`__
-  **Presentations:**
-  `GLU Course: About Version
   Control <https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit?usp=sharing>`__
-  **Third-party resources:**
-  What is `Git <https://git-scm.com>`__

   -  `Version
      control <https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control>`__
   -  `Getting Started - Git
      Basics <https://git-scm.com/book/en/v2/Getting-Started-Git-Basics>`__
   -  `Getting Started - Installing
      Git <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`__
   -  `Git on the Server -
      GitLab <https://git-scm.com/book/en/v2/Git-on-the-Server-GitLab>`__

Branching strategies
--------------------

-  **Articles:**
-  `GitLab Flow <https://about.gitlab.com/2014/09/29/gitlab-flow/>`__
-  **Third-party resources:**
-  `Git Branching - Branches in a
   Nutshell <https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell>`__
-  `Git Branching - Branching
   Workflows <https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows>`__

Advanced use
------------

-  `Custom Git Hooks <../../administration/custom_hooks.md>`__
-  `Git Attributes <../../user/project/git_attributes.md>`__
-  Git Submodules: `Using Git submodules with GitLab
   CI <../../ci/git_submodules.md#using-git-submodules-with-gitlab-ci>`__

API
---

-  `Gitignore templates <../../api/templates/gitignores.md>`__

Git LFS
-------

-  `Git
   LFS <../../workflow/lfs/manage_large_binaries_with_git_lfs.md>`__
-  `Git-Annex to Git-LFS migration
   guide <https://docs.gitlab.com/ee/workflow/lfs/migrate_from_git_annex_to_git_lfs.html>`__
-  **Articles:**
-  `Getting Started with Git
   LFS <https://about.gitlab.com/2017/01/30/getting-started-with-git-lfs-tutorial/>`__
-  `Towards a production quality open source Git LFS
   server <https://about.gitlab.com/2015/08/13/towards-a-production-quality-open-source-git-lfs-server/>`__

Troubleshooting
---------------

-  Learn a few `Git troubleshooting <troubleshooting_git.md>`__
   techniques to help you out.

General information
-------------------

-  **Articles:**
-  `The future of SaaS hosted Git repository
   pricing <https://about.gitlab.com/2016/05/11/git-repository-pricing/>`__
