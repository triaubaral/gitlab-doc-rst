Changing the appearance of the login page
=========================================

GitLab Community Edition offers a way to put your company's identity on
the login page of your GitLab server and make it a branded login page.

By default, the page shows the GitLab logo and description.

.. figure:: branded_login_page/default_login_page.png
   :alt: default\_login\_page

   default\_login\_page

Changing the appearance of the login page
-----------------------------------------

Navigate to the **Admin** area and go to the **Appearance** page.

Fill in the required details like Title, Description and upload the
company logo.

.. figure:: branded_login_page/appearance.png
   :alt: appearance

   appearance

After saving the page, your GitLab login page will have the details you
filled in:

.. figure:: branded_login_page/custom_sign_in.png
   :alt: company\_login\_page

   company\_login\_page
