Customize the complete sign-in page
===================================

Please see `Branded login page <branded_login_page.md>`__

Add a welcome message to the sign-in page (GitLab Community Edition)
====================================================================

It is possible to add a markdown-formatted welcome message to your
GitLab sign-in page. Users of GitLab Enterprise Edition should use the
`branded login page feature <branded_login_page.md>`__ instead.

The welcome message (extra\_sign\_in\_text) can now be set/changed in
the Admin UI. Admin area > Settings
