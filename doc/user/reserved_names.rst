Reserved project and group names
================================

Not all project & group names are allowed because they would conflict
with existing routes used by GitLab.

For a list of words that are not allowed to be used as group or project
names, see the ```path_regex.rb``
file <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/path_regex.rb>`__
under the ``TOP_LEVEL_ROUTES``, ``PROJECT_WILDCARD_ROUTES`` and
``GROUP_ROUTES`` lists: - ``TOP_LEVEL_ROUTES``: are names that are
reserved as usernames or top level groups - ``PROJECT_WILDCARD_ROUTES``:
are names that are reserved for child groups or projects. -
``GROUP_ROUTES``: are names that are reserved for all groups or
projects.

Reserved project names
----------------------

It is currently not possible to create a project with the following
names:

-  

   -  

-  badges
-  blame
-  blob
-  builds
-  commits
-  create
-  create\_dir
-  edit
-  environments/folders
-  files
-  find\_file
-  gitlab-lfs/objects
-  info/lfs/objects
-  new
-  preview
-  raw
-  refs
-  tree
-  update
-  wikis

Reserved group names
--------------------

Currently the following names are reserved as top level groups:

-  503.html
-  

   -  

-  .well-known
-  404.html
-  422.html
-  500.html
-  502.html
-  abuse\_reports
-  admin
-  api
-  apple-touch-icon-precomposed.png
-  apple-touch-icon.png
-  files
-  assets
-  autocomplete
-  ci
-  dashboard
-  deploy.html
-  explore
-  favicon.ico
-  groups
-  header\_logo\_dark.png
-  header\_logo\_light.png
-  health\_check
-  help
-  import
-  invites
-  jwt
-  koding
-  notification\_settings
-  oauth
-  profile
-  projects
-  public
-  robots.txt
-  s
-  search
-  sent\_notifications
-  slash-command-logo.png
-  snippets
-  u
-  unicorn\_test
-  unsubscribes
-  uploads
-  users

These group names are unavailable as subgroup names:

-  

   -  

-  activity
-  analytics
-  audit\_events
-  avatar
-  edit
-  group\_members
-  hooks
-  issues
-  labels
-  ldap
-  ldap\_group\_links
-  merge\_requests
-  milestones
-  notification\_setting
-  pipeline\_quota
-  projects
-  subgroups
