Personal access tokens
======================

    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/3749>`__
    in GitLab 8.8.

Personal access tokens are the preferred way for third party
applications and scripts to authenticate with the `GitLab
API <../../api/README.md>`__, if using `OAuth2 <../../api/oauth2.md>`__
is not practical.

You can also use them to authenticate against Git over HTTP. They are
the only accepted method of authentication when you have `Two-Factor
Authentication (2FA) <../account/two_factor_authentication.md>`__
enabled.

Once you have your token, `pass it to the
API <../../api/README.md#personal-access-tokens>`__ using either the
``private_token`` parameter or the ``Private-Token`` header.

The expiration of personal access tokens happens on the date you define,
at midnight UTC.

Creating a personal access token
--------------------------------

You can create as many personal access tokens as you like from your
GitLab profile.

1. Log in to your GitLab account.
2. Go to your **Profile settings**.
3. Go to **Access tokens**.
4. Choose a name and optionally an expiry date for the token.
5. Choose the `desired
   scopes <#limiting-scopes-of-a-personal-access-token>`__.
6. Click on **Create personal access token**.
7. Save the personal access token somewhere safe. Once you leave or
   refresh the page, you won't be able to access it again.

.. figure:: img/personal_access_tokens.png
   :alt: Personal access tokens page

   Personal access tokens page

Revoking a personal access token
--------------------------------

At any time, you can revoke any personal access token by just clicking
the respective **Revoke** button under the 'Active personal access
tokens' area.

Limiting scopes of a personal access token
------------------------------------------

Personal access tokens can be created with one or more scopes that allow
various actions that a given token can perform. The available scopes are
depicted in the following table.

+--------+--------------+
| Scope  | Description  |
+========+==============+
| ``read | Allows       |
| _user` | access to    |
| `      | the          |
|        | read-only    |
|        | endpoints    |
|        | under        |
|        | ``/users``.  |
|        | Essentially, |
|        | any of the   |
|        | ``GET``      |
|        | requests in  |
|        | the `Users   |
|        | API <../../a |
|        | pi/users.md> |
|        | `__          |
|        | are allowed  |
|        | (`introduced |
|        |  <https://gi |
|        | tlab.com/git |
|        | lab-org/gitl |
|        | ab-ce/merge_ |
|        | requests/595 |
|        | 1>`__        |
|        | in GitLab    |
|        | 8.15).       |
+--------+--------------+
| ``api` | Grants       |
| `      | complete     |
|        | access to    |
|        | the API      |
|        | (read/write) |
|        | (`introduced |
|        |  <https://gi |
|        | tlab.com/git |
|        | lab-org/gitl |
|        | ab-ce/merge_ |
|        | requests/595 |
|        | 1>`__        |
|        | in GitLab    |
|        | 8.15).       |
|        | Required for |
|        | accessing    |
|        | Git          |
|        | repositories |
|        | over HTTP    |
|        | when 2FA is  |
|        | enabled.     |
+--------+--------------+
| ``read | Allows to    |
| _regis | read         |
| try``  | `container   |
|        | registry <.. |
|        | /project/con |
|        | tainer_regis |
|        | try.md>`__   |
|        | images if a  |
|        | project is   |
|        | private and  |
|        | authorizatio |
|        | n            |
|        | is required  |
|        | (`introduced |
|        |  <https://gi |
|        | tlab.com/git |
|        | lab-org/gitl |
|        | ab-ce/merge_ |
|        | requests/118 |
|        | 45>`__       |
|        | in GitLab    |
|        | 9.3).        |
+--------+--------------+
| ``sudo | Allows       |
| ``     | performing   |
|        | API actions  |
|        | as any user  |
|        | in the       |
|        | system (if   |
|        | the          |
|        | authenticate |
|        | d            |
|        | user is an   |
|        | admin)       |
|        | (`introduced |
|        |  <https://gi |
|        | tlab.com/git |
|        | lab-org/gitl |
|        | ab-ce/merge_ |
|        | requests/148 |
|        | 38>`__       |
|        | in GitLab    |
|        | 10.2).       |
+--------+--------------+
