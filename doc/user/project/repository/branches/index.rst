Branches
========

Read through GiLab's branching documentation:

-  `Create a branch <../web_editor.md#create-a-new-branch>`__
-  `Default branch <#default-branch>`__
-  `Protected
   branches <../../protected_branches.md#protected-branches>`__
-  `Delete merged branches <#delete-merged-branches>`__

See also:

-  `GitLab
   Flow <../../../../university/training/gitlab_flow.md#gitlab-flow>`__:
   use the best of GitLab for your branching strategies
-  `Getting started with Git <../../../../topics/git/index.md>`__ and
   GitLab

Default branch
--------------

When you create a new `project <../../index.md>`__, GitLab sets
``master`` as the default branch for your project. You can choose
another branch to be your project's default under your project's
**Settings > General**.

The default branch is the branched affected by the `issue closing
pattern <../../issues/automatic_issue_closing.md>`__, which means that
*an issue will be closed when a merge request is merged to the **default
branch***.

The default branch is also protected against accidental deletion. Read
through the documentation on `protected
branches <../../protected_branches.md#protected-branches>`__ to learn
more.

Delete merged branches
----------------------

    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/6449>`__
    in GitLab 8.14.

.. figure:: img/delete_merged_branches.png
   :alt: Delete merged branches

   Delete merged branches

This feature allows merged branches to be deleted in bulk. Only branches
that have been merged and `are not
protected <../../protected_branches.md>`__ will be deleted as part of
this operation.

It's particularly useful to clean up old branches that were not deleted
automatically when a merge request was merged.
