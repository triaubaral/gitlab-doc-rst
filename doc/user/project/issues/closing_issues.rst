Closing Issues
==============

Please read through the `GitLab Issue Documentation <index.md>`__ for an
overview on GitLab Issues.

Directly
--------

Whenever you decide that's no longer need for that issue, close the
issue using the close button:

.. figure:: img/button_close_issue.png
   :alt: close issue - button

   close issue - button

Via Merge Request
-----------------

When a merge request resolves the discussion over an issue, you can make
it close that issue(s) when merged.

All you need is to use a `keyword <automatic_issue_closing.md>`__
accompanying the issue number, add to the description of that MR.

In this example, the keyword "closes" prefixing the issue number will
create a relationship in such a way that the merge request will close
the issue when merged.

Mentioning various issues in the same line also works for this purpose:

.. code:: md

    Closes #333, #444, #555 and #666

If the issue is in a different repository rather then the MR's, add the
full URL for that issue(s):

.. code:: md

    Closes #333, #444, and https://gitlab.com/<username>/<projectname>/issues/<xxx>

All the following keywords will produce the same behaviour:

-  Close, Closes, Closed, Closing, close, closes, closed, closing
-  Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
-  Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved,
   resolving

.. figure:: img/merge_request_closes_issue.png
   :alt: merge request closing issue when merged

   merge request closing issue when merged

If you use any other word before the issue number, the issue and the MR
will link to each other, but the MR will NOT close the issue(s) when
merged.

.. figure:: img/closing_and_related_issues.png
   :alt: mention issues in MRs - closing and related

   mention issues in MRs - closing and related

From the Issue Board
--------------------

You can close an issue from `Issue Boards <../issue_board.md>`__ by
draging an issue card from its list and dropping into **Closed**.

.. figure:: img/close_issue_from_board.gif
   :alt: close issue from the Issue Board

   close issue from the Issue Board

Customizing the issue closing patern
------------------------------------

Alternatively, a GitLab **administrator** can `customize the issue
closing patern <../../../administration/issue_closing_pattern.md>`__.
