Deleting Issues
===============

    [Introduced][ce-2982] in GitLab 8.6

Please read through the `GitLab Issue Documentation <index.md>`__ for an
overview on GitLab Issues.

You can delete an issue by editing it and clicking on the delete button.

.. figure:: img/delete_issue.png
   :alt: delete issue - button

   delete issue - button

    **Note:** Only `project owners <../../permissions.md>`__ can delete
    issues.
