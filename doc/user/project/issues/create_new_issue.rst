Create a new Issue
==================

Please read through the `GitLab Issue Documentation <index.md>`__ for an
overview on GitLab Issues.

When you create a new issue, you'll be prompted to fill in the
information illustrated on the image below.

.. figure:: img/new_issue.png
   :alt: New issue from the issues list

   New issue from the issues list

Read through the `issues functionalities
documentation <issues_functionalities.md#issues-functionalities>`__ to
understand these fields one by one.

New issue from the Issue Tracker
--------------------------------

Navigate to your **Project's Dashboard** > **Issues** > **New Issue** to
create a new issue:

.. figure:: img/new_issue_from_tracker_list.png
   :alt: New issue from the issue list view

   New issue from the issue list view

New issue from an opened issue
------------------------------

From an **opened issue** in your project, click **New Issue** to create
a new issue in the same project:

.. figure:: img/new_issue_from_open_issue.png
   :alt: New issue from an open issue

   New issue from an open issue

New issue from the project's dashboard
--------------------------------------

From your **Project's Dashboard**, click the plus sign (**+**) to open a
dropdown menu with a few options. Select **New Issue** to create an
issue in that project:

.. figure:: img/new_issue_from_projects_dashboard.png
   :alt: New issue from a project's dashboard

   New issue from a project's dashboard

New issue from the Issue Board
------------------------------

From an Issue Board, create a new issue by clicking on the plus sign
(**+**) on the top of a list. It opens a new issue for that project
labeled after its respective list.

.. figure:: img/new_issue_from_issue_board.png
   :alt: From the issue board

   From the issue board
