Due dates
=========

    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/3614>`__
    in GitLab 8.7.

Please read through the `GitLab Issue Documentation <index.md>`__ for an
overview on GitLab Issues.

Due dates can be used in issues to keep track of deadlines and make sure
features are shipped on time. Due dates require at least `Reporter
permissions <../../permissions.md#project>`__ to be able to edit them.
On the contrary, they can be seen by everybody.

Setting a due date
------------------

When creating or editing an issue, you can see the due date field from
where a calendar will appear to help you choose the date you want. To
remove it, select the date text and delete it.

.. figure:: img/due_dates_create.png
   :alt: Create a due date

   Create a due date

A quicker way to set a due date is via the issue sidebar. Simply expand
the sidebar and select **Edit** to pick a due date or remove the
existing one. Changes are saved immediately.

.. figure:: img/due_dates_edit_sidebar.png
   :alt: Edit a due date via the sidebar

   Edit a due date via the sidebar

Making use of due dates
-----------------------

Issues that have a due date can be distinctively seen in the issue
tracker displaying a date next to them. Issues where the date is overdue
will have the icon and the date colored red. You can sort issues by
those that are *Due soon* or *Due later* from the dropdown menu in the
right.

.. figure:: img/due_dates_issues_index_page.png
   :alt: Issues with due dates in the issues index page

   Issues with due dates in the issues index page

Due dates also appear in your `todos
list <../../../workflow/todos.md>`__.

.. figure:: img/due_dates_todos.png
   :alt: Issues with due dates in the todos

   Issues with due dates in the todos
