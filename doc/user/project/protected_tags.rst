Protected Tags
==============

    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10356>`__
    in GitLab 9.1.

Protected Tags allow control over who has permission to create tags as
well as preventing accidental update or deletion once created. Each rule
allows you to match either an individual tag name, or use wildcards to
control multiple tags at once.

This feature evolved out of `Protected
Branches <protected_branches.md>`__

Overview
--------

Protected tags will prevent anyone from updating or deleting the tag, as
and will prevent creation of matching tags based on the permissions you
have selected. By default, anyone without Master permission will be
prevented from creating tags.

Configuring protected tags
--------------------------

To protect a tag, you need to have at least Master permission level.

1. Navigate to the project's Settings -> Repository page

   .. figure:: img/project_repository_settings.png
      :alt: Repository Settings

      Repository Settings

2. From the **Tag** dropdown menu, select the tag you want to protect or
   type and click ``Create wildcard``. In the screenshot below, we chose
   to protect all tags matching ``v*``.

   .. figure:: img/protected_tags_page.png
      :alt: Protected tags page

      Protected tags page

3. From the ``Allowed to create`` dropdown, select who will have
   permission to create matching tags and then click ``Protect``.

   .. figure:: img/protected_tags_permissions_dropdown.png
      :alt: Allowed to create tags dropdown

      Allowed to create tags dropdown

4. Once done, the protected tag will appear in the "Protected tags"
   list.

   .. figure:: img/protected_tags_list.png
      :alt: Protected tags list

      Protected tags list

Wildcard protected tags
-----------------------

You can specify a wildcard protected tag, which will protect all tags
matching the wildcard. For example:

+--------------------------+-------------------------------------+
| Wildcard Protected Tag   | Matching Tags                       |
+==========================+=====================================+
| ``v*``                   | ``v1.0.0``, ``version-9.1``         |
+--------------------------+-------------------------------------+
| ``*-deploy``             | ``march-deploy``, ``1.0-deploy``    |
+--------------------------+-------------------------------------+
| ``*gitlab*``             | ``gitlab``, ``gitlab/v1``           |
+--------------------------+-------------------------------------+
| ``*``                    | ``v1.0.1rc2``, ``accidental-tag``   |
+--------------------------+-------------------------------------+

Two different wildcards can potentially match the same tag. For example,
``*-stable`` and ``production-*`` would both match a
``production-stable`` tag. In that case, if *any* of these protected
tags have a setting like "Allowed to create", then ``production-stable``
will also inherit this setting.

If you click on a protected tag's name, you will be presented with a
list of all matching tags:

.. figure:: img/protected_tag_matches.png
   :alt: Protected tag matches

   Protected tag matches

--------------
