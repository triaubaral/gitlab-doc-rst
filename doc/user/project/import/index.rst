Migrating projects to a GitLab instance
=======================================

1.  `From Bitbucket.org <bitbucket.md>`__
2.  `From ClearCase <clearcase.md>`__
3.  `From CVS <cvs.md>`__
4.  `From FogBugz <fogbugz.md>`__
5.  `From GitHub.com or GitHub Enterprise <github.md>`__
6.  `From GitLab.com <gitlab_com.md>`__
7.  `From Gitea <gitea.md>`__
8.  `From Perforce <perforce.md>`__
9.  `From SVN <svn.md>`__
10. `From TFS <tfs.md>`__

In addition to the specific migration documentation above, you can
import any Git repository via HTTP from the New Project page. Be aware
that if the repository is too large the import can timeout.

Migrating from self-hosted GitLab to GitLab.com
-----------------------------------------------

You can copy your repos by changing the remote and pushing to the new
server, but issues and merge requests can't be imported.

If you want to retain all metadata like issues and merge requests, you
can use the `import/export feature <../settings/import_export.md>`__.
