GitLab Pages from A to Z: Part 2
================================

    **Article
    `Type <../../../development/writing_documentation.html#types-of-technical-articles>`__**:
    user guide \|\| **Level**: beginner \|\| **Author**: `Marcia
    Ramos <https://gitlab.com/marcia>`__ \|\| **Publication date:**
    2017/02/22

-  `Part 1: Static sites and GitLab Pages
   domains <getting_started_part_one.md>`__
-  **Part 2: Quick start guide - Setting up GitLab Pages**
-  `Part 3: Setting Up Custom Domains - DNS Records and SSL/TLS
   Certificates <getting_started_part_three.md>`__
-  `Part 4: Creating and tweaking ``.gitlab-ci.yml`` for GitLab
   Pages <getting_started_part_four.md>`__

Setting up GitLab Pages
-----------------------

For a complete step-by-step tutorial, please read the blog post `Hosting
on GitLab.com with GitLab
Pages <https://about.gitlab.com/2016/04/07/gitlab-pages-setup/>`__. The
following sections will explain what do you need and why do you need
them.

What you need to get started
----------------------------

1. A project
2. A configuration file (``.gitlab-ci.yml``) to deploy your site
3. A specific ``job`` called ``pages`` in the configuration file that
   will make GitLab aware that you are deploying a GitLab Pages website

Optional Features:

1. A custom domain or subdomain
2. A DNS pointing your (sub)domain to your Pages site
3. **Optional**: an SSL/TLS certificate so your custom domain is
   accessible under HTTPS.

The optional settings, custom domain, DNS records, and SSL/TLS
certificates, are described in `Part
3 <getting_started_part_three.md>`__).

Project
-------

Your GitLab Pages project is a regular project created the same way you
do for the other ones. To get started with GitLab Pages, you have two
ways:

-  Fork one of the templates from Page Examples, or
-  Create a new project from scratch

Let's go over both options.

Fork a project to get started from
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To make things easy for you, we've created this
`group <https://gitlab.com/pages>`__ of default projects containing the
most popular SSGs templates.

Watch the `video tutorial <https://youtu.be/TWqh9MtT4Bg>`__ we've
created for the steps below.

1. Choose your SSG template
2. Fork a project from the `Pages group <https://gitlab.com/pages>`__
3. Remove the fork relationship by navigating to your **Project**'s
   **Settings** > **Edit Project**

   .. figure:: img/remove_fork_relashionship.png
      :alt: remove fork relashionship

      remove fork relashionship

4. Enable Shared Runners for your fork: navigate to your **Project**'s
   **Settings** > **Pipelines**
5. Trigger a build (push a change to any file)
6. As soon as the build passes, your website will have been deployed
   with GitLab Pages. Your website URL will be available under your
   **Project**'s **Settings** > **Pages**

To turn a **project website** forked from the Pages group into a
**user/group** website, you'll need to:

-  Rename it to ``namespace.gitlab.io``: navigate to **Project**'s
   **Settings** > **Edit Project** > **Rename repository**
-  Adjust your SSG's `base URL <#urls-and-baseurls>`__ to from
   ``"project-name"`` to ``""``. This setting will be at a different
   place for each SSG, as each of them have their own structure and file
   tree. Most likelly, it will be in the SSG's config file.

    **Notes:**

    1. Why do I need to remove the fork relationship?

       Unless you want to contribute to the original project, you won't
       need it connected to the upstream. A
       `fork <https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/#fork>`__
       is useful for submitting merge requests to the upstream.

    2. Why do I need to enable Shared Runners?

       Shared Runners will run the script set by your GitLab CI
       configuration file. They're enabled by default to new projects,
       but not to forks.

Create a project from scratch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. From your **Project**'s
   **`Dashboard <https://gitlab.com/dashboard/projects>`__**, click
   **New project**, and name it considering the `practical
   examples <getting_started_part_one.md#practical-examples>`__.
2. Clone it to your local computer, add your website files to your
   project, add, commit and push to GitLab.
3. From the your **Project**'s page, click **Set up CI**:

   .. figure:: img/setup_ci.png
      :alt: setup GitLab CI

      setup GitLab CI

4. Choose one of the templates from the dropbox menu. Pick up the
   template corresponding to the SSG you're using (or plain HTML).

   .. figure:: img/choose_ci_template.png
      :alt: gitlab-ci templates

      gitlab-ci templates

Once you have both site files and ``.gitlab-ci.yml`` in your project's
root, GitLab CI will build your site and deploy it with Pages. Once the
first build passes, you see your site is live by navigating to your
**Project**'s **Settings** > **Pages**, where you'll find its default
URL.

    **Notes:**

    -  GitLab Pages `supports any
       SSG <https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/>`__,
       but, if you don't find yours among the templates, you'll need to
       configure your own ``.gitlab-ci.yml``. Do do that, please read
       through the article `Creating and Tweaking ``.gitlab-ci.yml`` for
       GitLab Pages <getting_started_part_four.md>`__. New SSGs are very
       welcome among the `example
       projects <https://gitlab.com/pages>`__. If you set up a new one,
       please
       `contribute <https://gitlab.com/pages/pages.gitlab.io/blob/master/CONTRIBUTING.md>`__
       to our examples.

    -  The second step *"Clone it to your local computer"*, can be done
       differently, achieving the same results: instead of cloning the
       bare repository to you local computer and moving your site files
       into it, you can run ``git init`` in your local website
       directory, add the remote URL:
       ``git remote add origin git@gitlab.com:namespace/project-name.git``,
       then add, commit, and push.

URLs and Baseurls
~~~~~~~~~~~~~~~~~

Every Static Site Generator (SSG) default configuration expects to find
your website under a (sub)domain (``example.com``), not in a
subdirectory of that domain (``example.com/subdir``). Therefore,
whenever you publish a project website
(``namespace.gitlab.io/project-name``), you'll have to look for this
configuration (base URL) on your SSG's documentation and set it up to
reflect this pattern.

For example, for a Jekyll site, the ``baseurl`` is defined in the Jekyll
configuration file, ``_config.yml``. If your website URL is
``https://john.gitlab.io/blog/``, you need to add this line to
``_config.yml``:

.. code:: yaml

    baseurl: "/blog"

On the contrary, if you deploy your website after forking one of our
`default examples <https://gitlab.com/pages>`__, the baseurl will
already be configured this way, as all examples there are project
websites. If you decide to make yours a user or group website, you'll
have to remove this configuration from your project. For the Jekyll
example we've just mentioned, you'd have to change Jekyll's
``_config.yml`` to:

.. code:: yaml

    baseurl: ""

Custom Domains
~~~~~~~~~~~~~~

GitLab Pages supports custom domains and subdomains, served under HTTPS
or HTTPS. Please check the `next part <getting_started_part_three.md>`__
of this series for an overview.

+------+------+
| `**← | `**S |
| Part | etti |
| 1:   | ng   |
| Stat | Up   |
| ic   | Cust |
| site | om   |
| s,   | Doma |
| doma | ins  |
| ins, | -    |
| DNS  | DNS  |
| reco | Reco |
| rds, | rds  |
| and  | and  |
| SSL/ | SSL/ |
| TLS  | TLS  |
| cert | Cert |
| ific | ific |
| ates | ates |
| ** < | →**  |
| gett | <get |
| ing_ | ting |
| star | _sta |
| ted_ | rted |
| part | _par |
| _one | t_th |
| .md> | ree. |
| `__  | md>` |
|      | __   |
+------+------+
