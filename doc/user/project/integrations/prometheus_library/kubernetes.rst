Monitoring Kubernetes
=====================

    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/8935>`__
    in GitLab 9.0

GitLab has support for automatically detecting and monitoring Kubernetes
metrics.

Requirements
------------

The `Prometheus <../prometheus.md>`__ and
`Kubernetes <../kubernetes.md>`__ integration services must be enabled.

Metrics supported
-----------------

+-------+--------+
| Name  | Query  |
+=======+========+
| Avera | (sum(a |
| ge    | vg(con |
| Memor | tainer |
| y     | \_memo |
| Usage | ry\_us |
| (MB)  | age\_b |
|       | ytes{c |
|       | ontain |
|       | er\_na |
|       | me!="P |
|       | OD",en |
|       | vironm |
|       | ent="% |
|       | {ci\_e |
|       | nviron |
|       | ment\_ |
|       | slug}" |
|       | })     |
|       | withou |
|       | t      |
|       | (job)) |
|       | )      |
|       | /      |
|       | count( |
|       | avg(co |
|       | ntaine |
|       | r\_mem |
|       | ory\_u |
|       | sage\_ |
|       | bytes{ |
|       | contai |
|       | ner\_n |
|       | ame!=" |
|       | POD",e |
|       | nviron |
|       | ment=" |
|       | %{ci\_ |
|       | enviro |
|       | nment\ |
|       | _slug} |
|       | "})    |
|       | withou |
|       | t      |
|       | (job)) |
|       | /1024/ |
|       | 1024   |
+-------+--------+
| Avera | sum(av |
| ge    | g(rate |
| CPU   | (conta |
| Utili | iner\_ |
| zatio | cpu\_u |
| n     | sage\_ |
| (%)   | second |
|       | s\_tot |
|       | al{con |
|       | tainer |
|       | \_name |
|       | !="POD |
|       | ",envi |
|       | ronmen |
|       | t="%{c |
|       | i\_env |
|       | ironme |
|       | nt\_sl |
|       | ug}"}[ |
|       | 2m]))  |
|       | withou |
|       | t      |
|       | (job)) |
|       | \* 100 |
+-------+--------+

Configuring Prometheus to monitor for Kubernetes node metrics
-------------------------------------------------------------

In order for Prometheus to collect Kubernetes metrics, you first must
have a Prometheus server up and running. You have two options here:

-  If you have an Omnibus based GitLab installation within your
   Kubernetes cluster, you can leverage the bundled Prometheus server to
   `monitor
   Kubernetes <../../../../administration/monitoring/prometheus/index.md#configuring-prometheus-to-monitor-kubernetes>`__.
-  To configure your own Prometheus server, you can follow the
   `Prometheus
   documentation <https://prometheus.io/docs/introduction/overview/>`__
   or `our
   guide <../../../../administration/monitoring/prometheus/index.md#configuring-your-own-prometheus-server-within-kubernetes>`__.

Specifying the Environment label
--------------------------------

In order to isolate and only display relevant metrics for a given
environment however, GitLab needs a method to detect which labels are
associated. To do this, GitLab will `look for an ``environment``
label <metrics.md#identifying-environments>`__.

If you are using `GitLab
Auto-Deploy <../../../../ci/autodeploy/index.md>`__ and one of the two
`provided Kubernetes monitoring
solutions <../prometheus.md#getting-started-with-prometheus-monitoring>`__,
the ``environment`` label will be automatically added.
