Mattermost Notifications Service
================================

On Mattermost
-------------

To enable Mattermost integration you must create an incoming webhook
integration:

1. Sign in to your Mattermost instance
2. Visit incoming webhooks, that will be something like:
   https://mattermost.example/your\_team\_name/integrations/incoming\_webhooks/add
3. Choose a display name, description and channel, those can be
   overridden on GitLab
4. Save it, copy the **Webhook URL**, we'll need this later for GitLab.

There might be some cases that Incoming Webhooks are blocked by admin,
ask your mattermost admin to enable it on
https://mattermost.example/admin\_console/integrations/custom.

Display name override is not enabled by default, you need to ask your
admin to enable it on that same section.

On GitLab
---------

After you set up Mattermost, it's time to set up GitLab.

Navigate to the `Integrations
page <project_services.md#accessing-the-project-services>`__ and select
the **Mattermost notifications** service to configure it. There, you
will see a checkbox with the following events that can be triggered:

-  Push
-  Issue
-  Confidential issue
-  Merge request
-  Note
-  Tag push
-  Pipeline
-  Wiki page

Below each of these event checkboxes, you have an input field to enter
which Mattermost channel you want to send that event message. Enter your
preferred channel handle (the hash sign ``#`` is optional).

At the end, fill in your Mattermost details:

+--------+--------------+
| Field  | Description  |
+========+==============+
| **Webh | The incoming |
| ook**  | webhook URL  |
|        | which you    |
|        | have to      |
|        | setup on     |
|        | Mattermost,  |
|        | it will be   |
|        | something    |
|        | like:        |
|        | http://matte |
|        | rmost.exampl |
|        | e/hooks/5xo… |
+--------+--------------+
| **User | Optional     |
| name** | username     |
|        | which can be |
|        | on messages  |
|        | sent to      |
|        | Mattermost.  |
|        | Fill this in |
|        | if you want  |
|        | to change    |
|        | the username |
|        | of the bot.  |
+--------+--------------+
| **Noti | If you       |
| fy     | choose to    |
| only   | enable the   |
| broken | **Pipeline** |
| pipeli | event and    |
| nes**  | you want to  |
|        | be only      |
|        | notified     |
|        | about failed |
|        | pipelines.   |
+--------+--------------+

.. figure:: img/mattermost_configuration.png
   :alt: Mattermost configuration

   Mattermost configuration
