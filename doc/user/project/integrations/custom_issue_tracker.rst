Custom Issue Tracker Service
============================

To enable the Custom Issue Tracker integration in a project, navigate to
the `Integrations
page <project_services.md#accessing-the-project-services>`__, click the
**Customer Issue Tracker** service, and fill in the required details on
the page as described in the table below.

+--------+--------------+
| Field  | Description  |
+========+==============+
| ``titl | A title for  |
| e``    | the issue    |
|        | tracker (to  |
|        | differentiat |
|        | e            |
|        | between      |
|        | instances,   |
|        | for example) |
+--------+--------------+
| ``desc | A name for   |
| riptio | the issue    |
| n``    | tracker (to  |
|        | differentiat |
|        | e            |
|        | between      |
|        | instances,   |
|        | for example) |
+--------+--------------+
| ``proj | Currently    |
| ect_ur | unused. Will |
| l``    | be changed   |
|        | in a future  |
|        | release.     |
+--------+--------------+
| ``issu | The URL to   |
| es_url | the issue in |
| ``     | the issue    |
|        | tracker      |
|        | project that |
|        | is linked to |
|        | this GitLab  |
|        | project.     |
|        | Note that    |
|        | the          |
|        | ``issues_url |
|        | ``           |
|        | requires     |
|        | ``:id`` in   |
|        | the URL.     |
|        | This ID is   |
|        | used by      |
|        | GitLab as a  |
|        | placeholder  |
|        | to replace   |
|        | the issue    |
|        | number. For  |
|        | example,     |
|        | ``https://cu |
|        | stomissuetra |
|        | cker.com/pro |
|        | ject-name/:i |
|        | d``.         |
+--------+--------------+
| ``new_ | Currently    |
| issue_ | unused. Will |
| url``  | be changed   |
|        | in a future  |
|        | release.     |
+--------+--------------+

Referencing issues
------------------

Issues are referenced with ``#<ID>``, where ``<ID>`` is a number
(example ``#143``). So with the example above, ``#143`` would refer to
``https://customissuetracker.com/project-name/143``.
