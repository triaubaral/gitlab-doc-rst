Project services
================

Project services allow you to integrate GitLab with other applications.
They are a bit like plugins in that they allow a lot of freedom in
adding functionality to GitLab.

Accessing the project services
------------------------------

You can find the available services under your project's **Settings ➔
Integrations** page.

There are more than 20 services to integrate with. Click on the one that
you want to configure.

.. figure:: img/project_services.png
   :alt: Project services list

   Project services list

Below, you will find a list of the currently supported ones accompanied
with comprehensive documentation.

Services
--------

Click on the service links to see further configuration instructions and
details.

+----------+--------------+
| Service  | Description  |
+==========+==============+
| Asana    | Asana -      |
|          | Teamwork     |
|          | without      |
|          | email        |
+----------+--------------+
| Assembla | Project      |
|          | Management   |
|          | Software     |
|          | (Source      |
|          | Commits      |
|          | Endpoint)    |
+----------+--------------+
| `Atlassi | A continuous |
| an       | integration  |
| Bamboo   | and build    |
| CI <bamb | server       |
| oo.md>`_ |              |
| _        |              |
+----------+--------------+
| Buildkit | Continuous   |
| e        | integration  |
|          | and          |
|          | deployments  |
+----------+--------------+
| `Bugzill | Bugzilla     |
| a <bugzi | issue        |
| lla.md>` | tracker      |
| __       |              |
+----------+--------------+
| Campfire | Simple       |
|          | web-based    |
|          | real-time    |
|          | group chat   |
+----------+--------------+
| Custom   | Custom issue |
| Issue    | tracker      |
| Tracker  |              |
+----------+--------------+
| Drone CI | Continuous   |
|          | Integration  |
|          | platform     |
|          | built on     |
|          | Docker,      |
|          | written in   |
|          | Go           |
+----------+--------------+
| `Emails  | Email the    |
| on       | commits and  |
| push <em | diff of each |
| ails_on_ | push to a    |
| push.md> | list of      |
| `__      | recipients   |
+----------+--------------+
| External | Replaces the |
| Wiki     | link to the  |
|          | internal     |
|          | wiki with a  |
|          | link to an   |
|          | external     |
|          | wiki         |
+----------+--------------+
| Flowdock | Flowdock is  |
|          | a            |
|          | collaboratio |
|          | n            |
|          | web app for  |
|          | technical    |
|          | teams        |
+----------+--------------+
| Gemnasiu | Gemnasium    |
| m        | monitors     |
|          | your project |
|          | dependencies |
|          | and alerts   |
|          | you about    |
|          | updates and  |
|          | security     |
|          | vulnerabilit |
|          | ies          |
+----------+--------------+
| `HipChat | Private      |
|  <hipcha | group chat   |
| t.md>`__ | and IM       |
+----------+--------------+
| `Irker   | Send IRC     |
| (IRC     | messages, on |
| gateway) | update, to a |
|  <irker. | list of      |
| md>`__   | recipients   |
|          | through an   |
|          | Irker        |
|          | gateway      |
+----------+--------------+
| `JIRA <j | JIRA issue   |
| ira.md>` | tracker      |
| __       |              |
+----------+--------------+
| JetBrain | A continuous |
| s        | integration  |
| TeamCity | and build    |
| CI       | server       |
+----------+--------------+
| `Kuberne | A            |
| tes <kub | containerize |
| ernetes. | d            |
| md>`__   | deployment   |
| *(Has    | service      |
| been     |              |
| deprecat |              |
| ed       |              |
| in       |              |
| GitLab   |              |
| 10.4)*   |              |
+----------+--------------+
| `Matterm | Mattermost   |
| ost      | chat and     |
| slash    | ChatOps      |
| commands | slash        |
|  <matter | commands     |
| most_sla |              |
| sh_comma |              |
| nds.md>` |              |
| __       |              |
+----------+--------------+
| `Matterm | Receive      |
| ost      | event        |
| Notifica | notification |
| tions <m | s            |
| attermos | in           |
| t.md>`__ | Mattermost   |
+----------+--------------+
| `Microso | Receive      |
| ft       | notification |
| teams <m | s            |
| icrosoft | for actions  |
| _teams.m | that happen  |
| d>`__    | on GitLab    |
|          | into a room  |
|          | on Microsoft |
|          | Teams using  |
|          | Office 365   |
|          | Connectors   |
+----------+--------------+
| Packagis | Update your  |
| t        | project on   |
|          | Packagist,   |
|          | the main     |
|          | Composer     |
|          | repository   |
+----------+--------------+
| Pipeline | Email the    |
| s        | pipeline     |
| emails   | status to a  |
|          | list of      |
|          | recipients   |
+----------+--------------+
| `Slack   | Send GitLab  |
| Notifica | events (e.g. |
| tions <s | issue        |
| lack.md> | created) to  |
| `__      | Slack as     |
|          | notification |
|          | s            |
+----------+--------------+
| `Slack   | Use slash    |
| slash    | commands in  |
| commands | Slack to     |
|  <slack_ | control      |
| slash_co | GitLab       |
| mmands.m |              |
| d>`__    |              |
+----------+--------------+
| PivotalT | Project      |
| racker   | Management   |
|          | Software     |
|          | (Source      |
|          | Commits      |
|          | Endpoint)    |
+----------+--------------+
| `Prometh | Monitor the  |
| eus <pro | performance  |
| metheus. | of your      |
| md>`__   | deployed     |
|          | apps         |
+----------+--------------+
| Pushover | Pushover     |
|          | makes it     |
|          | easy to get  |
|          | real-time    |
|          | notification |
|          | s            |
|          | on your      |
|          | Android      |
|          | device,      |
|          | iPhone,      |
|          | iPad, and    |
|          | Desktop      |
+----------+--------------+
| `Redmine | Redmine      |
|  <redmin | issue        |
| e.md>`__ | tracker      |
+----------+--------------+

Services templates
------------------

Services templates is a way to set some predefined values in the Service
of your liking which will then be pre-filled on each project's Service.

Read more about `Services templates in this
document <services_templates.md>`__.

Contributing to project services
--------------------------------

Because GitLab is open source we can ship with the code and tests for
all plugins. This allows the community to keep the plugins up to date so
that they always work in newer GitLab versions.

For an overview of what projects services are available, please see the
`project\_services source
directory <https://gitlab.com/gitlab-org/gitlab-ce/tree/master/app/models/project_services>`__.

Contributions are welcome!
