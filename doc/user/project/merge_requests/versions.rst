Merge requests versions
=======================

    **Notes:** -
    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/5467>`__
    in GitLab 8.12. - Comments are disabled while viewing outdated merge
    versions or comparing to versions other than base. - Merge request
    versions are based on push not on commit. So, if you pushed 5
    commits in a single push, it will be a single option in the
    dropdown. If you pushed 5 times, that will count for 5 options.

Every time you push to a branch that is tied to a merge request, a new
version of merge request diff is created. When you visit a merge request
that contains more than one pushes, you can select and compare the
versions of those merge request diffs.

.. figure:: img/versions.png
   :alt: Merge request versions

   Merge request versions

--------------

By default, the latest version of changes is shown. However, you can
select an older one from version dropdown.

.. figure:: img/versions_dropdown.png
   :alt: Merge request versions dropdown

   Merge request versions dropdown

--------------

You can also compare the merge request version with an older one to see
what has changed since then.

.. figure:: img/versions_compare.png
   :alt: Merge request versions compare

   Merge request versions compare

--------------

Every time you push new changes to the branch, a link to compare the
last changes appears as a system note.

.. figure:: img/versions_system_note.png
   :alt: Merge request versions system note

   Merge request versions system note
