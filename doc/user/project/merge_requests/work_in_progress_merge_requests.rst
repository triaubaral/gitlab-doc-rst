"Work In Progress" Merge Requests
=================================

To prevent merge requests from accidentally being accepted before
they're completely ready, GitLab blocks the "Accept" button for merge
requests that have been marked a **Work In Progress**.

.. figure:: img/wip_blocked_accept_button.png
   :alt: Blocked Accept Button

   Blocked Accept Button

To mark a merge request a Work In Progress, simply start its title with
``[WIP]`` or ``WIP:``.

.. figure:: img/wip_mark_as_wip.png
   :alt: Mark as WIP

   Mark as WIP

To allow a Work In Progress merge request to be accepted again when it's
ready, simply remove the ``WIP`` prefix.

.. figure:: img/wip_unmark_as_wip.png
   :alt: Unark as WIP

   Unark as WIP
