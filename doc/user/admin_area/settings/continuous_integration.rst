Continuous integration Admin settings
=====================================

Maximum artifacts size
----------------------

The maximum size of the `job
artifacts <../../../administration/job_artifacts.md>`__ can be set in
the Admin area of your GitLab instance. The value is in *MB* and the
default is 100MB. Note that this setting is set for each job.

1. Go to **Admin area > Settings** (``/admin/application_settings``).

   .. figure:: img/admin_area_settings_button.png
      :alt: Admin area settings button

      Admin area settings button

2. Change the value of maximum artifacts size (in MB):

   .. figure:: img/admin_area_maximum_artifacts_size.png
      :alt: Admin area maximum artifacts size

      Admin area maximum artifacts size

3. Hit **Save** for the changes to take effect.

Default artifacts expiration
----------------------------

The default expiration time of the `job
artifacts <../../../administration/job_artifacts.md>`__ can be set in
the Admin area of your GitLab instance. The syntax of duration is
described in
`artifacts:expire\_in <../../../ci/yaml/README.md#artifactsexpire_in>`__.
The default is ``30 days``. Note that this setting is set for each job.
Set it to 0 if you don't want default expiration.

1. Go to **Admin area > Settings** (``/admin/application_settings``).

   .. figure:: img/admin_area_settings_button.png
      :alt: Admin area settings button

      Admin area settings button

2. Change the value of default expiration time
   (`syntax <../../../ci/yaml/README.md#artifactsexpire_in>`__):

   .. figure:: img/admin_area_default_artifacts_expiration.png
      :alt: Admin area default artifacts expiration

      Admin area default artifacts expiration

3. Hit **Save** for the changes to take effect.
