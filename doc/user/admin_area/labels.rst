Labels
======

Default Labels
--------------

Define your own default Label Set
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Labels that are created within the Labels view on the Admin Dashboard
will be automatically added to each new project.

.. figure:: img/admin_labels.png
   :alt: Default label set

   Default label set
