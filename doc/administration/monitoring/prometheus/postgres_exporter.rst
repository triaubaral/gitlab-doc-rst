Postgres exporter
=================

    **Note:** Available since `Omnibus GitLab
    8.17 <https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests/1131>`__.
    For installations from source you'll have to install and configure
    it yourself.

The `postgres
exporter <https://github.com/wrouesnel/postgres_exporter>`__ allows you
to measure various PostgreSQL metrics.

To enable the postgres exporter:

1. `Enable Prometheus <index.md#configuring-prometheus>`__
2. Edit ``/etc/gitlab/gitlab.rb``
3. Add or find and uncomment the following line, making sure it's set to
   ``true``:

   .. code:: ruby

       postgres_exporter['enable'] = true

4. Save the file and `reconfigure
   GitLab <../../restart_gitlab.md#omnibus-gitlab-reconfigure>`__ for
   the changes to take effect

Prometheus will now automatically begin collecting performance data from
the postgres exporter exposed under ``localhost:9187``.

`← Back to the main Prometheus page <index.md>`__
