Redis exporter
==============

    **Note:** Available since `Omnibus GitLab
    8.17 <https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests/1118>`__.
    For installations from source you'll have to install and configure
    it yourself.

The `Redis exporter <https://github.com/oliver006/redis_exporter>`__
allows you to measure various `Redis <https://redis.io>`__ metrics. For
more information on what's exported `read the upstream
documentation <https://github.com/oliver006/redis_exporter/blob/master/README.md#whats-exported>`__.

To enable the Redis exporter:

1. `Enable Prometheus <index.md#configuring-prometheus>`__
2. Edit ``/etc/gitlab/gitlab.rb``
3. Add or find and uncomment the following line, making sure it's set to
   ``true``:

   .. code:: ruby

       redis_exporter['enable'] = true

4. Save the file and `reconfigure
   GitLab <../../restart_gitlab.md#omnibus-gitlab-reconfigure>`__ for
   the changes to take effect

Prometheus will now automatically begin collecting performance data from
the Redis exporter exposed under ``localhost:9121``.

`← Back to the main Prometheus page <index.md>`__
