Atlassian Crowd OmniAuth Provider
=================================

Configure a new Crowd application
---------------------------------

1. Choose 'Applications' in the top menu, then 'Add application'.
2. Go through the 'Add application' steps, entering the appropriate
   details. The screenshot below shows an example configuration.

   .. figure:: img/crowd_application.png
      :alt: Example Crowd application configuration

      Example Crowd application configuration

Configure GitLab
----------------

1. On your GitLab server, open the configuration file.

   **Omnibus:**

   .. code:: sh

         sudo editor /etc/gitlab/gitlab.rb

   **Source:**

   .. code:: sh

         cd /home/git/gitlab

         sudo -u git -H editor config/gitlab.yml

2. See `Initial OmniAuth
   Configuration <../../integration/omniauth.md#initial-omniauth-configuration>`__
   for initial settings.

3. Add the provider configuration:

   **Omnibus:**

   .. code:: ruby

         gitlab_rails['omniauth_providers'] = [
           {
             "name" => "crowd",
             "args" => {
               "crowd_server_url" => "CROWD_SERVER_URL",
               "application_name" => "YOUR_APP_NAME",
               "application_password" => "YOUR_APP_PASSWORD"
             }
           }
         ]

   **Source:**

   ::

          - { name: 'crowd',
              args: {
                crowd_server_url: 'CROWD_SERVER_URL',
                application_name: 'YOUR_APP_NAME',
                application_password: 'YOUR_APP_PASSWORD' } }

4. Change ``CROWD_SERVER_URL`` to the URL of your Crowd server.
5. Change ``YOUR_APP_NAME`` to the application name from Crowd
   applications page.
6. Change ``YOUR_APP_PASSWORD`` to the application password you've set.
7. Save the configuration file.
8. `Reconfigure <../restart_gitlab.md#omnibus-gitlab-reconfigure>`__ or
   `restart <../restart_gitlab.md#installations-from-source>`__ for the
   changes to take effect if you installed GitLab via Omnibus or from
   source respectively.

On the sign in page there should now be a Crowd tab in the sign in form.
