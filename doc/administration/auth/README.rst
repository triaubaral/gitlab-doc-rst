Authentication and Authorization
================================

GitLab integrates with the following external authentication and
authorization providers.

-  `LDAP <ldap.md>`__ Includes Active Directory, Apple Open Directory,
   Open LDAP, and 389 Server
-  `OmniAuth <../../integration/omniauth.md>`__ Sign in via Twitter,
   GitHub, GitLab.com, Google, Bitbucket, Facebook, Shibboleth, Crowd,
   Azure and Authentiq ID
-  `CAS <../../integration/cas.md>`__ Configure GitLab to sign in using
   CAS
-  `SAML <../../integration/saml.md>`__ Configure GitLab as a SAML 2.0
   Service Provider
-  `Okta <okta.md>`__ Configure GitLab to sign in using Okta
-  `Authentiq <authentiq.md>`__: Enable the Authentiq OmniAuth provider
   for passwordless authentication
