Namespaces API
==============

Usernames and groupnames fall under a special category called
namespaces.

For users and groups supported API calls see the `users <users.md>`__
and `groups <groups.md>`__ documentation respectively.

`Pagination <README.md#pagination>`__ is used.

List namespaces
---------------

Get a list of the namespaces of the authenticated user. If the user is
an administrator, a list of all namespaces in the GitLab instance is
shown.

::

    GET /namespaces

Example request:

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/namespaces

Example response:

.. code:: json

    [
      {
        "id": 1,
        "name": "user1",
        "path": "user1",
        "kind": "user",
        "full_path": "user1"
      },
      {
        "id": 2,
        "name": "group1",
        "path": "group1",
        "kind": "group",
        "full_path": "group1",
        "parent_id": "null",
        "members_count_with_descendants": 2
      },
      {
        "id": 3,
        "name": "bar",
        "path": "bar",
        "kind": "group",
        "full_path": "foo/bar",
        "parent_id": "9",
        "members_count_with_descendants": 5
      }
    ]

**Note**: ``members_count_with_descendants`` are presented only for
group masters/owners.

Search for namespace
--------------------

Get all namespaces that match a string in their name or path.

::

    GET /namespaces?search=foobar

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``search`` | strin | no        | Returns a    |
|            | g     |           | list of      |
|            |       |           | namespaces   |
|            |       |           | the user is  |
|            |       |           | authorized   |
|            |       |           | to see based |
|            |       |           | on the       |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+

Example request:

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/namespaces?search=twitter

Example response:

.. code:: json

    [
      {
        "id": 4,
        "name": "twitter",
        "path": "twitter",
        "kind": "group",
        "full_path": "twitter",
        "parent_id": "null",
        "members_count_with_descendants": 2
      }
    ]

Get namespace by ID
-------------------

Get a namespace by ID.

::

    GET /namespaces/:id

+-------------+------------------+------------+-------------------------------+
| Attribute   | Type             | Required   | Description                   |
+=============+==================+============+===============================+
| ``id``      | integer/string   | yes        | ID or path of the namespace   |
+-------------+------------------+------------+-------------------------------+

Example request:

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/namespaces/2

Example response:

.. code:: json

    {
      "id": 2,
      "name": "group1",
      "path": "group1",
      "kind": "group",
      "full_path": "group1",
      "parent_id": "null",
      "members_count_with_descendants": 2
    }

Example request:

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/namespaces/group1

Example response:

.. code:: json

    {
      "id": 2,
      "name": "group1",
      "path": "group1",
      "kind": "group",
      "full_path": "group1",
      "parent_id": "null",
      "members_count_with_descendants": 2
    }
