Features flags API
==================

All methods require administrator authorization.

Notice that currently the API only supports boolean and
percentage-of-time gate values.

List all features
-----------------

Get a list of all persisted features, with its gate values.

::

    GET /features

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/features

Example response:

.. code:: json

    [
      {
        "name": "experimental_feature",
        "state": "off",
        "gates": [
          {
            "key": "boolean",
            "value": false
          }
        ]
      },
      {
        "name": "new_library",
        "state": "on",
        "gates": [
          {
            "key": "boolean",
            "value": true
          }
        ]
      }
    ]

Set or create a feature
-----------------------

Set a feature's gate value. If a feature with the given name doesn't
exist yet it will be created. The value can be a boolean, or an integer
to indicate percentage of time.

::

    POST /features/:name

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``name``   | strin | yes       | Name of the  |
|            | g     |           | feature to   |
|            |       |           | create or    |
|            |       |           | update       |
+------------+-------+-----------+--------------+
| ``value``  | integ | yes       | ``true`` or  |
|            | er/st |           | ``false`` to |
|            | ring  |           | enable/disab |
|            |       |           | le,          |
|            |       |           | or an        |
|            |       |           | integer for  |
|            |       |           | percentage   |
|            |       |           | of time      |
+------------+-------+-----------+--------------+
| ``feature_ | strin | no        | A Feature    |
| group``    | g     |           | group name   |
+------------+-------+-----------+--------------+
| ``user``   | strin | no        | A GitLab     |
|            | g     |           | username     |
+------------+-------+-----------+--------------+

Note that you can enable or disable a feature for both a
``feature_group`` and a ``user`` with a single API call.

.. code:: bash

    curl --data "value=30" --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/features/new_library

Example response:

.. code:: json

    {
      "name": "new_library",
      "state": "conditional",
      "gates": [
        {
          "key": "boolean",
          "value": false
        },
        {
          "key": "percentage_of_time",
          "value": 30
        }
      ]
    }
