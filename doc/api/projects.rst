Projects API
============

Project visibility level
------------------------

Project in GitLab can be either private, internal or public. This is
determined by the ``visibility`` field in the project.

Values for the project visibility level are:

-  ``private``: Project access must be granted explicitly for each user.

-  ``internal``: The project can be cloned by any logged in user.

-  ``public``: The project can be cloned without any authentication.

List all projects
-----------------

Get a list of all visible projects across GitLab for the authenticated
user. When accessed without authentication, only public projects are
returned.

::

    GET /projects

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``archived | boole | no        | Limit by     |
| ``         | an    |           | archived     |
|            |       |           | status       |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | Limit by     |
| ty``       | g     |           | visibility   |
|            |       |           | ``public``,  |
|            |       |           | ``internal`` |
|            |       |           | ,            |
|            |       |           | or           |
|            |       |           | ``private``  |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Return       |
| ``         | g     |           | projects     |
|            |       |           | ordered by   |
|            |       |           | ``id``,      |
|            |       |           | ``name``,    |
|            |       |           | ``path``,    |
|            |       |           | ``created_at |
|            |       |           | ``,          |
|            |       |           | ``updated_at |
|            |       |           | ``,          |
|            |       |           | or           |
|            |       |           | ``last_activ |
|            |       |           | ity_at``     |
|            |       |           | fields.      |
|            |       |           | Default is   |
|            |       |           | ``created_at |
|            |       |           | ``           |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Return       |
|            | g     |           | projects     |
|            |       |           | sorted in    |
|            |       |           | ``asc`` or   |
|            |       |           | ``desc``     |
|            |       |           | order.       |
|            |       |           | Default is   |
|            |       |           | ``desc``     |
+------------+-------+-----------+--------------+
| ``search`` | strin | no        | Return list  |
|            | g     |           | of projects  |
|            |       |           | matching the |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+
| ``simple`` | boole | no        | Return only  |
|            | an    |           | the ID, URL, |
|            |       |           | name, and    |
|            |       |           | path of each |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``owned``  | boole | no        | Limit by     |
|            | an    |           | projects     |
|            |       |           | owned by the |
|            |       |           | current user |
+------------+-------+-----------+--------------+
| ``membersh | boole | no        | Limit by     |
| ip``       | an    |           | projects     |
|            |       |           | that the     |
|            |       |           | current user |
|            |       |           | is a member  |
|            |       |           | of           |
+------------+-------+-----------+--------------+
| ``starred` | boole | no        | Limit by     |
| `          | an    |           | projects     |
|            |       |           | starred by   |
|            |       |           | the current  |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``statisti | boole | no        | Include      |
| cs``       | an    |           | project      |
|            |       |           | statistics   |
+------------+-------+-----------+--------------+
| ``with_iss | boole | no        | Limit by     |
| ues_enable | an    |           | enabled      |
| d``        |       |           | issues       |
|            |       |           | feature      |
+------------+-------+-----------+--------------+
| ``with_mer | boole | no        | Limit by     |
| ge_request | an    |           | enabled      |
| s_enabled` |       |           | merge        |
| `          |       |           | requests     |
|            |       |           | feature      |
+------------+-------+-----------+--------------+

.. code:: json

    [
      {
        "id": 4,
        "description": null,
        "default_branch": "master",
        "visibility": "private",
        "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
        "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
        "web_url": "http://example.com/diaspora/diaspora-client",
        "tag_list": [
          "example",
          "disapora client"
        ],
        "owner": {
          "id": 3,
          "name": "Diaspora",
          "created_at": "2013-09-30T13:46:02Z"
        },
        "name": "Diaspora Client",
        "name_with_namespace": "Diaspora / Diaspora Client",
        "path": "diaspora-client",
        "path_with_namespace": "diaspora/diaspora-client",
        "issues_enabled": true,
        "open_issues_count": 1,
        "merge_requests_enabled": true,
        "jobs_enabled": true,
        "wiki_enabled": true,
        "snippets_enabled": false,
        "resolve_outdated_diff_discussions": false,
        "container_registry_enabled": false,
        "created_at": "2013-09-30T13:46:02Z",
        "last_activity_at": "2013-09-30T13:46:02Z",
        "creator_id": 3,
        "namespace": {
          "id": 3,
          "name": "Diaspora",
          "path": "diaspora",
          "kind": "group",
          "full_path": "diaspora"
        },
        "import_status": "none",
        "archived": false,
        "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
        "shared_runners_enabled": true,
        "forks_count": 0,
        "star_count": 0,
        "runners_token": "b8547b1dc37721d05889db52fa2f02",
        "public_jobs": true,
        "shared_with_groups": [],
        "only_allow_merge_if_pipeline_succeeds": false,
        "only_allow_merge_if_all_discussions_are_resolved": false,
        "request_access_enabled": false,
        "statistics": {
          "commit_count": 37,
          "storage_size": 1038090,
          "repository_size": 1038090,
          "lfs_objects_size": 0,
          "job_artifacts_size": 0
        },
        "_links": {
          "self": "http://example.com/api/v4/projects",
          "issues": "http://example.com/api/v4/projects/1/issues",
          "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
          "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
          "labels": "http://example.com/api/v4/projects/1/labels",
          "events": "http://example.com/api/v4/projects/1/events",
          "members": "http://example.com/api/v4/projects/1/members"
        },
      },
      {
        "id": 6,
        "description": null,
        "default_branch": "master",
        "visibility": "private",
        "ssh_url_to_repo": "git@example.com:brightbox/puppet.git",
        "http_url_to_repo": "http://example.com/brightbox/puppet.git",
        "web_url": "http://example.com/brightbox/puppet",
        "tag_list": [
          "example",
          "puppet"
        ],
        "owner": {
          "id": 4,
          "name": "Brightbox",
          "created_at": "2013-09-30T13:46:02Z"
        },
        "name": "Puppet",
        "name_with_namespace": "Brightbox / Puppet",
        "path": "puppet",
        "path_with_namespace": "brightbox/puppet",
        "issues_enabled": true,
        "open_issues_count": 1,
        "merge_requests_enabled": true,
        "jobs_enabled": true,
        "wiki_enabled": true,
        "snippets_enabled": false,
        "resolve_outdated_diff_discussions": false,
        "container_registry_enabled": false,
        "created_at": "2013-09-30T13:46:02Z",
        "last_activity_at": "2013-09-30T13:46:02Z",
        "creator_id": 3,
        "namespace": {
          "id": 4,
          "name": "Brightbox",
          "path": "brightbox",
          "kind": "group",
          "full_path": "brightbox"
        },
        "import_status": "none",
        "import_error": null,
        "permissions": {
          "project_access": {
            "access_level": 10,
            "notification_level": 3
          },
          "group_access": {
            "access_level": 50,
            "notification_level": 3
          }
        },
        "archived": false,
        "avatar_url": null,
        "shared_runners_enabled": true,
        "forks_count": 0,
        "star_count": 0,
        "runners_token": "b8547b1dc37721d05889db52fa2f02",
        "public_jobs": true,
        "shared_with_groups": [],
        "only_allow_merge_if_pipeline_succeeds": false,
        "only_allow_merge_if_all_discussions_are_resolved": false,
        "request_access_enabled": false,
        "statistics": {
          "commit_count": 12,
          "storage_size": 2066080,
          "repository_size": 2066080,
          "lfs_objects_size": 0,
          "job_artifacts_size": 0
        },
        "_links": {
          "self": "http://example.com/api/v4/projects",
          "issues": "http://example.com/api/v4/projects/1/issues",
          "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
          "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
          "labels": "http://example.com/api/v4/projects/1/labels",
          "events": "http://example.com/api/v4/projects/1/events",
          "members": "http://example.com/api/v4/projects/1/members"
        }
      }
    ]

You can filter by `custom attributes <custom_attributes.md>`__ with:

::

    GET /projects?custom_attributes[key]=value&custom_attributes[other_key]=other_value

List user projects
------------------

Get a list of visible projects for the given user. When accessed without
authentication, only public projects are returned.

::

    GET /users/:user_id/projects

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``user_id` | strin | yes       | The ID or    |
| `          | g     |           | username of  |
|            |       |           | the user     |
+------------+-------+-----------+--------------+
| ``archived | boole | no        | Limit by     |
| ``         | an    |           | archived     |
|            |       |           | status       |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | Limit by     |
| ty``       | g     |           | visibility   |
|            |       |           | ``public``,  |
|            |       |           | ``internal`` |
|            |       |           | ,            |
|            |       |           | or           |
|            |       |           | ``private``  |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Return       |
| ``         | g     |           | projects     |
|            |       |           | ordered by   |
|            |       |           | ``id``,      |
|            |       |           | ``name``,    |
|            |       |           | ``path``,    |
|            |       |           | ``created_at |
|            |       |           | ``,          |
|            |       |           | ``updated_at |
|            |       |           | ``,          |
|            |       |           | or           |
|            |       |           | ``last_activ |
|            |       |           | ity_at``     |
|            |       |           | fields.      |
|            |       |           | Default is   |
|            |       |           | ``created_at |
|            |       |           | ``           |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Return       |
|            | g     |           | projects     |
|            |       |           | sorted in    |
|            |       |           | ``asc`` or   |
|            |       |           | ``desc``     |
|            |       |           | order.       |
|            |       |           | Default is   |
|            |       |           | ``desc``     |
+------------+-------+-----------+--------------+
| ``search`` | strin | no        | Return list  |
|            | g     |           | of projects  |
|            |       |           | matching the |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+
| ``simple`` | boole | no        | Return only  |
|            | an    |           | the ID, URL, |
|            |       |           | name, and    |
|            |       |           | path of each |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``owned``  | boole | no        | Limit by     |
|            | an    |           | projects     |
|            |       |           | owned by the |
|            |       |           | current user |
+------------+-------+-----------+--------------+
| ``membersh | boole | no        | Limit by     |
| ip``       | an    |           | projects     |
|            |       |           | that the     |
|            |       |           | current user |
|            |       |           | is a member  |
|            |       |           | of           |
+------------+-------+-----------+--------------+
| ``starred` | boole | no        | Limit by     |
| `          | an    |           | projects     |
|            |       |           | starred by   |
|            |       |           | the current  |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``statisti | boole | no        | Include      |
| cs``       | an    |           | project      |
|            |       |           | statistics   |
+------------+-------+-----------+--------------+
| ``with_iss | boole | no        | Limit by     |
| ues_enable | an    |           | enabled      |
| d``        |       |           | issues       |
|            |       |           | feature      |
+------------+-------+-----------+--------------+
| ``with_mer | boole | no        | Limit by     |
| ge_request | an    |           | enabled      |
| s_enabled` |       |           | merge        |
| `          |       |           | requests     |
|            |       |           | feature      |
+------------+-------+-----------+--------------+

.. code:: json

    [
      {
        "id": 4,
        "description": null,
        "default_branch": "master",
        "visibility": "private",
        "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
        "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
        "web_url": "http://example.com/diaspora/diaspora-client",
        "tag_list": [
          "example",
          "disapora client"
        ],
        "owner": {
          "id": 3,
          "name": "Diaspora",
          "created_at": "2013-09-30T13:46:02Z"
        },
        "name": "Diaspora Client",
        "name_with_namespace": "Diaspora / Diaspora Client",
        "path": "diaspora-client",
        "path_with_namespace": "diaspora/diaspora-client",
        "issues_enabled": true,
        "open_issues_count": 1,
        "merge_requests_enabled": true,
        "jobs_enabled": true,
        "wiki_enabled": true,
        "snippets_enabled": false,
        "resolve_outdated_diff_discussions": false,
        "container_registry_enabled": false,
        "created_at": "2013-09-30T13:46:02Z",
        "last_activity_at": "2013-09-30T13:46:02Z",
        "creator_id": 3,
        "namespace": {
          "id": 3,
          "name": "Diaspora",
          "path": "diaspora",
          "kind": "group",
          "full_path": "diaspora"
        },
        "import_status": "none",
        "archived": false,
        "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
        "shared_runners_enabled": true,
        "forks_count": 0,
        "star_count": 0,
        "runners_token": "b8547b1dc37721d05889db52fa2f02",
        "public_jobs": true,
        "shared_with_groups": [],
        "only_allow_merge_if_pipeline_succeeds": false,
        "only_allow_merge_if_all_discussions_are_resolved": false,
        "request_access_enabled": false,
        "statistics": {
          "commit_count": 37,
          "storage_size": 1038090,
          "repository_size": 1038090,
          "lfs_objects_size": 0,
          "job_artifacts_size": 0
        },
        "_links": {
          "self": "http://example.com/api/v4/projects",
          "issues": "http://example.com/api/v4/projects/1/issues",
          "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
          "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
          "labels": "http://example.com/api/v4/projects/1/labels",
          "events": "http://example.com/api/v4/projects/1/events",
          "members": "http://example.com/api/v4/projects/1/members"
        }
      },
      {
        "id": 6,
        "description": null,
        "default_branch": "master",
        "visibility": "private",
        "ssh_url_to_repo": "git@example.com:brightbox/puppet.git",
        "http_url_to_repo": "http://example.com/brightbox/puppet.git",
        "web_url": "http://example.com/brightbox/puppet",
        "tag_list": [
          "example",
          "puppet"
        ],
        "owner": {
          "id": 4,
          "name": "Brightbox",
          "created_at": "2013-09-30T13:46:02Z"
        },
        "name": "Puppet",
        "name_with_namespace": "Brightbox / Puppet",
        "path": "puppet",
        "path_with_namespace": "brightbox/puppet",
        "issues_enabled": true,
        "open_issues_count": 1,
        "merge_requests_enabled": true,
        "jobs_enabled": true,
        "wiki_enabled": true,
        "snippets_enabled": false,
        "resolve_outdated_diff_discussions": false,
        "container_registry_enabled": false,
        "created_at": "2013-09-30T13:46:02Z",
        "last_activity_at": "2013-09-30T13:46:02Z",
        "creator_id": 3,
        "namespace": {
          "id": 4,
          "name": "Brightbox",
          "path": "brightbox",
          "kind": "group",
          "full_path": "brightbox"
        },
        "import_status": "none",
        "import_error": null,
        "permissions": {
          "project_access": {
            "access_level": 10,
            "notification_level": 3
          },
          "group_access": {
            "access_level": 50,
            "notification_level": 3
          }
        },
        "archived": false,
        "avatar_url": null,
        "shared_runners_enabled": true,
        "forks_count": 0,
        "star_count": 0,
        "runners_token": "b8547b1dc37721d05889db52fa2f02",
        "public_jobs": true,
        "shared_with_groups": [],
        "only_allow_merge_if_pipeline_succeeds": false,
        "only_allow_merge_if_all_discussions_are_resolved": false,
        "request_access_enabled": false,
        "statistics": {
          "commit_count": 12,
          "storage_size": 2066080,
          "repository_size": 2066080,
          "lfs_objects_size": 0,
          "job_artifacts_size": 0
        },
        "_links": {
          "self": "http://example.com/api/v4/projects",
          "issues": "http://example.com/api/v4/projects/1/issues",
          "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
          "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
          "labels": "http://example.com/api/v4/projects/1/labels",
          "events": "http://example.com/api/v4/projects/1/events",
          "members": "http://example.com/api/v4/projects/1/members"
        }
      }
    ]

Get single project
------------------

Get a specific project. This endpoint can be accessed without
authentication if the project is publicly accessible.

::

    GET /projects/:id

+------------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute        | Type             | Required   | Description                                                                          |
+==================+==================+============+======================================================================================+
| ``id``           | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+------------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``statistics``   | boolean          | no         | Include project statistics                                                           |
+------------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: json

    {
      "id": 3,
      "description": null,
      "default_branch": "master",
      "visibility": "private",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site",
      "tag_list": [
        "example",
        "disapora project"
      ],
      "owner": {
        "id": 3,
        "name": "Diaspora",
        "created_at": "2013-09-30T13:46:02Z"
      },
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "issues_enabled": true,
      "open_issues_count": 1,
      "merge_requests_enabled": true,
      "jobs_enabled": true,
      "wiki_enabled": true,
      "snippets_enabled": false,
      "resolve_outdated_diff_discussions": false,
      "container_registry_enabled": false,
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "creator_id": 3,
      "namespace": {
        "id": 3,
        "name": "Diaspora",
        "path": "diaspora",
        "kind": "group",
        "full_path": "diaspora"
      },
      "import_status": "none",
      "import_error": null,
      "permissions": {
        "project_access": {
          "access_level": 10,
          "notification_level": 3
        },
        "group_access": {
          "access_level": 50,
          "notification_level": 3
        }
      },
      "archived": false,
      "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
      "shared_runners_enabled": true,
      "forks_count": 0,
      "star_count": 0,
      "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
      "public_jobs": true,
      "shared_with_groups": [
        {
          "group_id": 4,
          "group_name": "Twitter",
          "group_access_level": 30
        },
        {
          "group_id": 3,
          "group_name": "Gitlab Org",
          "group_access_level": 10
        }
      ],
      "only_allow_merge_if_pipeline_succeeds": false,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "printing_merge_requests_link_enabled": true,
      "request_access_enabled": false,
      "statistics": {
        "commit_count": 37,
        "storage_size": 1038090,
        "repository_size": 1038090,
        "lfs_objects_size": 0,
        "job_artifacts_size": 0
      },
      "_links": {
        "self": "http://example.com/api/v4/projects",
        "issues": "http://example.com/api/v4/projects/1/issues",
        "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
        "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
        "labels": "http://example.com/api/v4/projects/1/labels",
        "events": "http://example.com/api/v4/projects/1/events",
        "members": "http://example.com/api/v4/projects/1/members"
      }
    }

Get project users
-----------------

Get the users list of a project.

::

    GET /projects/:id/users

+--------------+----------+------------+-----------------------------+
| Attribute    | Type     | Required   | Description                 |
+==============+==========+============+=============================+
| ``search``   | string   | no         | Search for specific users   |
+--------------+----------+------------+-----------------------------+

.. code:: json

    [
      {
        "id": 1,
        "username": "john_smith",
        "name": "John Smith",
        "state": "active",
        "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
        "web_url": "http://localhost:3000/john_smith"
      },
      {
        "id": 2,
        "username": "jack_smith",
        "name": "Jack Smith",
        "state": "blocked",
        "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
        "web_url": "http://localhost:3000/jack_smith"
      }
    ]

Get project events
------------------

Please refer to the `Events API
documentation <events.md#list-a-projects-visible-events>`__.

Create project
--------------

Creates a new project owned by the authenticated user.

::

    POST /projects

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``name``   | strin | yes if    | The name of  |
|            | g     | path is   | the new      |
|            |       | not       | project.     |
|            |       | provided  | Equals path  |
|            |       |           | if not       |
|            |       |           | provided.    |
+------------+-------+-----------+--------------+
| ``path``   | strin | yes if    | Repository   |
|            | g     | name is   | name for new |
|            |       | not       | project.     |
|            |       | provided  | Generated    |
|            |       |           | based on     |
|            |       |           | name if not  |
|            |       |           | provided     |
|            |       |           | (generated   |
|            |       |           | lowercased   |
|            |       |           | with         |
|            |       |           | dashes).     |
+------------+-------+-----------+--------------+
| ``namespac | integ | no        | Namespace    |
| e_id``     | er    |           | for the new  |
|            |       |           | project      |
|            |       |           | (defaults to |
|            |       |           | the current  |
|            |       |           | user's       |
|            |       |           | namespace)   |
+------------+-------+-----------+--------------+
| ``descript | strin | no        | Short        |
| ion``      | g     |           | project      |
|            |       |           | description  |
+------------+-------+-----------+--------------+
| ``issues_e | boole | no        | Enable       |
| nabled``   | an    |           | issues for   |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``merge_re | boole | no        | Enable merge |
| quests_ena | an    |           | requests for |
| bled``     |       |           | this project |
+------------+-------+-----------+--------------+
| ``jobs_ena | boole | no        | Enable jobs  |
| bled``     | an    |           | for this     |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``wiki_ena | boole | no        | Enable wiki  |
| bled``     | an    |           | for this     |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``snippets | boole | no        | Enable       |
| _enabled`` | an    |           | snippets for |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``resolve_ | boole | no        | Automaticall |
| outdated_d | an    |           | y            |
| iff_discus |       |           | resolve      |
| sions``    |       |           | merge        |
|            |       |           | request      |
|            |       |           | diffs        |
|            |       |           | discussions  |
|            |       |           | on lines     |
|            |       |           | changed with |
|            |       |           | a push       |
+------------+-------+-----------+--------------+
| ``containe | boole | no        | Enable       |
| r_registry | an    |           | container    |
| _enabled`` |       |           | registry for |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``shared_r | boole | no        | Enable       |
| unners_ena | an    |           | shared       |
| bled``     |       |           | runners for  |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | See `project |
| ty``       | g     |           | visibility   |
|            |       |           | level <#proj |
|            |       |           | ect-visibili |
|            |       |           | ty-level>`__ |
+------------+-------+-----------+--------------+
| ``import_u | strin | no        | URL to       |
| rl``       | g     |           | import       |
|            |       |           | repository   |
|            |       |           | from         |
+------------+-------+-----------+--------------+
| ``public_j | boole | no        | If ``true``, |
| obs``      | an    |           | jobs can be  |
|            |       |           | viewed by    |
|            |       |           | non-project- |
|            |       |           | members      |
+------------+-------+-----------+--------------+
| ``only_all | boole | no        | Set whether  |
| ow_merge_i | an    |           | merge        |
| f_pipeline |       |           | requests can |
| _succeeds` |       |           | only be      |
| `          |       |           | merged with  |
|            |       |           | successful   |
|            |       |           | jobs         |
+------------+-------+-----------+--------------+
| ``only_all | boole | no        | Set whether  |
| ow_merge_i | an    |           | merge        |
| f_all_disc |       |           | requests can |
| ussions_ar |       |           | only be      |
| e_resolved |       |           | merged when  |
| ``         |       |           | all the      |
|            |       |           | discussions  |
|            |       |           | are resolved |
+------------+-------+-----------+--------------+
| ``lfs_enab | boole | no        | Enable LFS   |
| led``      | an    |           |              |
+------------+-------+-----------+--------------+
| ``request_ | boole | no        | Allow users  |
| access_ena | an    |           | to request   |
| bled``     |       |           | member       |
|            |       |           | access       |
+------------+-------+-----------+--------------+
| ``tag_list | array | no        | The list of  |
| ``         |       |           | tags for a   |
|            |       |           | project; put |
|            |       |           | array of     |
|            |       |           | tags, that   |
|            |       |           | should be    |
|            |       |           | finally      |
|            |       |           | assigned to  |
|            |       |           | a project    |
+------------+-------+-----------+--------------+
| ``avatar`` | mixed | no        | Image file   |
|            |       |           | for avatar   |
|            |       |           | of the       |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``printing | boole | no        | Show link to |
| _merge_req | an    |           | create/view  |
| uest_link_ |       |           | merge        |
| enabled``  |       |           | request when |
|            |       |           | pushing from |
|            |       |           | the command  |
|            |       |           | line         |
+------------+-------+-----------+--------------+
| ``ci_confi | strin | no        | The path to  |
| g_path``   | g     |           | CI config    |
|            |       |           | file         |
+------------+-------+-----------+--------------+

Create project for user
-----------------------

Creates a new project owned by the specified user. Available only for
admins.

::

    POST /projects/user/:user_id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``user_id` | integ | yes       | The user ID  |
| `          | er    |           | of the       |
|            |       |           | project      |
|            |       |           | owner        |
+------------+-------+-----------+--------------+
| ``name``   | strin | yes       | The name of  |
|            | g     |           | the new      |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``path``   | strin | no        | Custom       |
|            | g     |           | repository   |
|            |       |           | name for new |
|            |       |           | project. By  |
|            |       |           | default      |
|            |       |           | generated    |
|            |       |           | based on     |
|            |       |           | name         |
+------------+-------+-----------+--------------+
| ``default_ | strin | no        | ``master``   |
| branch``   | g     |           | by default   |
+------------+-------+-----------+--------------+
| ``namespac | integ | no        | Namespace    |
| e_id``     | er    |           | for the new  |
|            |       |           | project      |
|            |       |           | (defaults to |
|            |       |           | the current  |
|            |       |           | user's       |
|            |       |           | namespace)   |
+------------+-------+-----------+--------------+
| ``descript | strin | no        | Short        |
| ion``      | g     |           | project      |
|            |       |           | description  |
+------------+-------+-----------+--------------+
| ``issues_e | boole | no        | Enable       |
| nabled``   | an    |           | issues for   |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``merge_re | boole | no        | Enable merge |
| quests_ena | an    |           | requests for |
| bled``     |       |           | this project |
+------------+-------+-----------+--------------+
| ``jobs_ena | boole | no        | Enable jobs  |
| bled``     | an    |           | for this     |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``wiki_ena | boole | no        | Enable wiki  |
| bled``     | an    |           | for this     |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``snippets | boole | no        | Enable       |
| _enabled`` | an    |           | snippets for |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``resolve_ | boole | no        | Automaticall |
| outdated_d | an    |           | y            |
| iff_discus |       |           | resolve      |
| sions``    |       |           | merge        |
|            |       |           | request      |
|            |       |           | diffs        |
|            |       |           | discussions  |
|            |       |           | on lines     |
|            |       |           | changed with |
|            |       |           | a push       |
+------------+-------+-----------+--------------+
| ``containe | boole | no        | Enable       |
| r_registry | an    |           | container    |
| _enabled`` |       |           | registry for |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``shared_r | boole | no        | Enable       |
| unners_ena | an    |           | shared       |
| bled``     |       |           | runners for  |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | See `project |
| ty``       | g     |           | visibility   |
|            |       |           | level <#proj |
|            |       |           | ect-visibili |
|            |       |           | ty-level>`__ |
+------------+-------+-----------+--------------+
| ``import_u | strin | no        | URL to       |
| rl``       | g     |           | import       |
|            |       |           | repository   |
|            |       |           | from         |
+------------+-------+-----------+--------------+
| ``public_j | boole | no        | If ``true``, |
| obs``      | an    |           | jobs can be  |
|            |       |           | viewed by    |
|            |       |           | non-project- |
|            |       |           | members      |
+------------+-------+-----------+--------------+
| ``only_all | boole | no        | Set whether  |
| ow_merge_i | an    |           | merge        |
| f_pipeline |       |           | requests can |
| _succeeds` |       |           | only be      |
| `          |       |           | merged with  |
|            |       |           | successful   |
|            |       |           | jobs         |
+------------+-------+-----------+--------------+
| ``only_all | boole | no        | Set whether  |
| ow_merge_i | an    |           | merge        |
| f_all_disc |       |           | requests can |
| ussions_ar |       |           | only be      |
| e_resolved |       |           | merged when  |
| ``         |       |           | all the      |
|            |       |           | discussions  |
|            |       |           | are resolved |
+------------+-------+-----------+--------------+
| ``lfs_enab | boole | no        | Enable LFS   |
| led``      | an    |           |              |
+------------+-------+-----------+--------------+
| ``request_ | boole | no        | Allow users  |
| access_ena | an    |           | to request   |
| bled``     |       |           | member       |
|            |       |           | access       |
+------------+-------+-----------+--------------+
| ``tag_list | array | no        | The list of  |
| ``         |       |           | tags for a   |
|            |       |           | project; put |
|            |       |           | array of     |
|            |       |           | tags, that   |
|            |       |           | should be    |
|            |       |           | finally      |
|            |       |           | assigned to  |
|            |       |           | a project    |
+------------+-------+-----------+--------------+
| ``avatar`` | mixed | no        | Image file   |
|            |       |           | for avatar   |
|            |       |           | of the       |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``printing | boole | no        | Show link to |
| _merge_req | an    |           | create/view  |
| uest_link_ |       |           | merge        |
| enabled``  |       |           | request when |
|            |       |           | pushing from |
|            |       |           | the command  |
|            |       |           | line         |
+------------+-------+-----------+--------------+
| ``ci_confi | strin | no        | The path to  |
| g_path``   | g     |           | CI config    |
|            |       |           | file         |
+------------+-------+-----------+--------------+

Edit project
------------

Updates an existing project.

::

    PUT /projects/:id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
+------------+-------+-----------+--------------+
| ``name``   | strin | yes       | The name of  |
|            | g     |           | the project  |
+------------+-------+-----------+--------------+
| ``path``   | strin | no        | Custom       |
|            | g     |           | repository   |
|            |       |           | name for the |
|            |       |           | project. By  |
|            |       |           | default      |
|            |       |           | generated    |
|            |       |           | based on     |
|            |       |           | name         |
+------------+-------+-----------+--------------+
| ``default_ | strin | no        | ``master``   |
| branch``   | g     |           | by default   |
+------------+-------+-----------+--------------+
| ``descript | strin | no        | Short        |
| ion``      | g     |           | project      |
|            |       |           | description  |
+------------+-------+-----------+--------------+
| ``issues_e | boole | no        | Enable       |
| nabled``   | an    |           | issues for   |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``merge_re | boole | no        | Enable merge |
| quests_ena | an    |           | requests for |
| bled``     |       |           | this project |
+------------+-------+-----------+--------------+
| ``jobs_ena | boole | no        | Enable jobs  |
| bled``     | an    |           | for this     |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``wiki_ena | boole | no        | Enable wiki  |
| bled``     | an    |           | for this     |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``snippets | boole | no        | Enable       |
| _enabled`` | an    |           | snippets for |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``resolve_ | boole | no        | Automaticall |
| outdated_d | an    |           | y            |
| iff_discus |       |           | resolve      |
| sions``    |       |           | merge        |
|            |       |           | request      |
|            |       |           | diffs        |
|            |       |           | discussions  |
|            |       |           | on lines     |
|            |       |           | changed with |
|            |       |           | a push       |
+------------+-------+-----------+--------------+
| ``containe | boole | no        | Enable       |
| r_registry | an    |           | container    |
| _enabled`` |       |           | registry for |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``shared_r | boole | no        | Enable       |
| unners_ena | an    |           | shared       |
| bled``     |       |           | runners for  |
|            |       |           | this project |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | See `project |
| ty``       | g     |           | visibility   |
|            |       |           | level <#proj |
|            |       |           | ect-visibili |
|            |       |           | ty-level>`__ |
+------------+-------+-----------+--------------+
| ``import_u | strin | no        | URL to       |
| rl``       | g     |           | import       |
|            |       |           | repository   |
|            |       |           | from         |
+------------+-------+-----------+--------------+
| ``public_j | boole | no        | If ``true``, |
| obs``      | an    |           | jobs can be  |
|            |       |           | viewed by    |
|            |       |           | non-project- |
|            |       |           | members      |
+------------+-------+-----------+--------------+
| ``only_all | boole | no        | Set whether  |
| ow_merge_i | an    |           | merge        |
| f_pipeline |       |           | requests can |
| _succeeds` |       |           | only be      |
| `          |       |           | merged with  |
|            |       |           | successful   |
|            |       |           | jobs         |
+------------+-------+-----------+--------------+
| ``only_all | boole | no        | Set whether  |
| ow_merge_i | an    |           | merge        |
| f_all_disc |       |           | requests can |
| ussions_ar |       |           | only be      |
| e_resolved |       |           | merged when  |
| ``         |       |           | all the      |
|            |       |           | discussions  |
|            |       |           | are resolved |
+------------+-------+-----------+--------------+
| ``lfs_enab | boole | no        | Enable LFS   |
| led``      | an    |           |              |
+------------+-------+-----------+--------------+
| ``request_ | boole | no        | Allow users  |
| access_ena | an    |           | to request   |
| bled``     |       |           | member       |
|            |       |           | access       |
+------------+-------+-----------+--------------+
| ``tag_list | array | no        | The list of  |
| ``         |       |           | tags for a   |
|            |       |           | project; put |
|            |       |           | array of     |
|            |       |           | tags, that   |
|            |       |           | should be    |
|            |       |           | finally      |
|            |       |           | assigned to  |
|            |       |           | a project    |
+------------+-------+-----------+--------------+
| ``avatar`` | mixed | no        | Image file   |
|            |       |           | for avatar   |
|            |       |           | of the       |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``ci_confi | strin | no        | The path to  |
| g_path``   | g     |           | CI config    |
|            |       |           | file         |
+------------+-------+-----------+--------------+

Fork project
------------

Forks a project into the user namespace of the authenticated user or the
one provided.

The forking operation for a project is asynchronous and is completed in
a background job. The request will return immediately. To determine
whether the fork of the project has completed, query the
``import_status`` for the new project.

::

    POST /projects/:id/fork

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
+------------+-------+-----------+--------------+
| ``namespac | integ | yes       | The ID or    |
| e``        | er/st |           | path of the  |
|            | ring  |           | namespace    |
|            |       |           | that the     |
|            |       |           | project will |
|            |       |           | be forked to |
+------------+-------+-----------+--------------+

List Forks of a project
-----------------------

    **Note:** This feature was introduced in GitLab 10.1

List the projects accessible to the calling user that have an
established, forked relationship with the specified project

::

    GET /projects/:id/forks

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
+------------+-------+-----------+--------------+
| ``archived | boole | no        | Limit by     |
| ``         | an    |           | archived     |
|            |       |           | status       |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | Limit by     |
| ty``       | g     |           | visibility   |
|            |       |           | ``public``,  |
|            |       |           | ``internal`` |
|            |       |           | ,            |
|            |       |           | or           |
|            |       |           | ``private``  |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Return       |
| ``         | g     |           | projects     |
|            |       |           | ordered by   |
|            |       |           | ``id``,      |
|            |       |           | ``name``,    |
|            |       |           | ``path``,    |
|            |       |           | ``created_at |
|            |       |           | ``,          |
|            |       |           | ``updated_at |
|            |       |           | ``,          |
|            |       |           | or           |
|            |       |           | ``last_activ |
|            |       |           | ity_at``     |
|            |       |           | fields.      |
|            |       |           | Default is   |
|            |       |           | ``created_at |
|            |       |           | ``           |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Return       |
|            | g     |           | projects     |
|            |       |           | sorted in    |
|            |       |           | ``asc`` or   |
|            |       |           | ``desc``     |
|            |       |           | order.       |
|            |       |           | Default is   |
|            |       |           | ``desc``     |
+------------+-------+-----------+--------------+
| ``search`` | strin | no        | Return list  |
|            | g     |           | of projects  |
|            |       |           | matching the |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+
| ``simple`` | boole | no        | Return only  |
|            | an    |           | the ID, URL, |
|            |       |           | name, and    |
|            |       |           | path of each |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``owned``  | boole | no        | Limit by     |
|            | an    |           | projects     |
|            |       |           | owned by the |
|            |       |           | current user |
+------------+-------+-----------+--------------+
| ``membersh | boole | no        | Limit by     |
| ip``       | an    |           | projects     |
|            |       |           | that the     |
|            |       |           | current user |
|            |       |           | is a member  |
|            |       |           | of           |
+------------+-------+-----------+--------------+
| ``starred` | boole | no        | Limit by     |
| `          | an    |           | projects     |
|            |       |           | starred by   |
|            |       |           | the current  |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``statisti | boole | no        | Include      |
| cs``       | an    |           | project      |
|            |       |           | statistics   |
+------------+-------+-----------+--------------+
| ``with_iss | boole | no        | Limit by     |
| ues_enable | an    |           | enabled      |
| d``        |       |           | issues       |
|            |       |           | feature      |
+------------+-------+-----------+--------------+
| ``with_mer | boole | no        | Limit by     |
| ge_request | an    |           | enabled      |
| s_enabled` |       |           | merge        |
| `          |       |           | requests     |
|            |       |           | feature      |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" "https://gitlab.example.com/api/v4/projects/5/forks"

Example responses:

.. code:: json

    [
      {
        "id": 3,
        "description": null,
        "default_branch": "master",
        "visibility": "internal",
        "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
        "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
        "web_url": "http://example.com/diaspora/diaspora-project-site",
        "tag_list": [
          "example",
          "disapora project"
        ],
        "name": "Diaspora Project Site",
        "name_with_namespace": "Diaspora / Diaspora Project Site",
        "path": "diaspora-project-site",
        "path_with_namespace": "diaspora/diaspora-project-site",
        "issues_enabled": true,
        "open_issues_count": 1,
        "merge_requests_enabled": true,
        "jobs_enabled": true,
        "wiki_enabled": true,
        "snippets_enabled": false,
        "resolve_outdated_diff_discussions": false,
        "container_registry_enabled": false,
        "created_at": "2013-09-30T13:46:02Z",
        "last_activity_at": "2013-09-30T13:46:02Z",
        "creator_id": 3,
        "namespace": {
          "id": 3,
          "name": "Diaspora",
          "path": "diaspora",
          "kind": "group",
          "full_path": "diaspora"
        },
        "import_status": "none",
        "archived": true,
        "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
        "shared_runners_enabled": true,
        "forks_count": 0,
        "star_count": 1,
        "public_jobs": true,
        "shared_with_groups": [],
        "only_allow_merge_if_pipeline_succeeds": false,
        "only_allow_merge_if_all_discussions_are_resolved": false,
        "request_access_enabled": false,
        "_links": {
          "self": "http://example.com/api/v4/projects",
          "issues": "http://example.com/api/v4/projects/1/issues",
          "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
          "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
          "labels": "http://example.com/api/v4/projects/1/labels",
          "events": "http://example.com/api/v4/projects/1/events",
          "members": "http://example.com/api/v4/projects/1/members"
        }
      }
    ]

Star a project
--------------

Stars a given project. Returns status code ``304`` if the project is
already starred.

::

    POST /projects/:id/star

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" "https://gitlab.example.com/api/v4/projects/5/star"

Example response:

.. code:: json

    {
      "id": 3,
      "description": null,
      "default_branch": "master",
      "visibility": "internal",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site",
      "tag_list": [
        "example",
        "disapora project"
      ],
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "issues_enabled": true,
      "open_issues_count": 1,
      "merge_requests_enabled": true,
      "jobs_enabled": true,
      "wiki_enabled": true,
      "snippets_enabled": false,
      "resolve_outdated_diff_discussions": false,
      "container_registry_enabled": false,
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "creator_id": 3,
      "namespace": {
        "id": 3,
        "name": "Diaspora",
        "path": "diaspora",
        "kind": "group",
        "full_path": "diaspora"
      },
      "import_status": "none",
      "archived": true,
      "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
      "shared_runners_enabled": true,
      "forks_count": 0,
      "star_count": 1,
      "public_jobs": true,
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "request_access_enabled": false,
      "_links": {
        "self": "http://example.com/api/v4/projects",
        "issues": "http://example.com/api/v4/projects/1/issues",
        "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
        "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
        "labels": "http://example.com/api/v4/projects/1/labels",
        "events": "http://example.com/api/v4/projects/1/events",
        "members": "http://example.com/api/v4/projects/1/members"
      }
    }

Unstar a project
----------------

Unstars a given project. Returns status code ``304`` if the project is
not starred.

::

    POST /projects/:id/unstar

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" "https://gitlab.example.com/api/v4/projects/5/unstar"

Example response:

.. code:: json

    {
      "id": 3,
      "description": null,
      "default_branch": "master",
      "visibility": "internal",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site",
      "tag_list": [
        "example",
        "disapora project"
      ],
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "issues_enabled": true,
      "open_issues_count": 1,
      "merge_requests_enabled": true,
      "jobs_enabled": true,
      "wiki_enabled": true,
      "snippets_enabled": false,
      "resolve_outdated_diff_discussions": false,
      "container_registry_enabled": false,
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "creator_id": 3,
      "namespace": {
        "id": 3,
        "name": "Diaspora",
        "path": "diaspora",
        "kind": "group",
        "full_path": "diaspora"
      },
      "import_status": "none",
      "archived": true,
      "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
      "shared_runners_enabled": true,
      "forks_count": 0,
      "star_count": 0,
      "public_jobs": true,
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "request_access_enabled": false,
      "_links": {
        "self": "http://example.com/api/v4/projects",
        "issues": "http://example.com/api/v4/projects/1/issues",
        "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
        "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
        "labels": "http://example.com/api/v4/projects/1/labels",
        "events": "http://example.com/api/v4/projects/1/events",
        "members": "http://example.com/api/v4/projects/1/members"
      }
    }

Archive a project
-----------------

Archives the project if the user is either admin or the project owner of
this project. This action is idempotent, thus archiving an already
archived project will not change the project.

::

    POST /projects/:id/archive

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" "https://gitlab.example.com/api/v4/projects/5/archive"

Example response:

.. code:: json

    {
      "id": 3,
      "description": null,
      "default_branch": "master",
      "visibility": "private",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site",
      "tag_list": [
        "example",
        "disapora project"
      ],
      "owner": {
        "id": 3,
        "name": "Diaspora",
        "created_at": "2013-09-30T13:46:02Z"
      },
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "issues_enabled": true,
      "open_issues_count": 1,
      "merge_requests_enabled": true,
      "jobs_enabled": true,
      "wiki_enabled": true,
      "snippets_enabled": false,
      "resolve_outdated_diff_discussions": false,
      "container_registry_enabled": false,
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "creator_id": 3,
      "namespace": {
        "id": 3,
        "name": "Diaspora",
        "path": "diaspora",
        "kind": "group",
        "full_path": "diaspora"
      },
      "import_status": "none",
      "import_error": null,
      "permissions": {
        "project_access": {
          "access_level": 10,
          "notification_level": 3
        },
        "group_access": {
          "access_level": 50,
          "notification_level": 3
        }
      },
      "archived": true,
      "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
      "shared_runners_enabled": true,
      "forks_count": 0,
      "star_count": 0,
      "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
      "public_jobs": true,
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "request_access_enabled": false,
      "_links": {
        "self": "http://example.com/api/v4/projects",
        "issues": "http://example.com/api/v4/projects/1/issues",
        "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
        "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
        "labels": "http://example.com/api/v4/projects/1/labels",
        "events": "http://example.com/api/v4/projects/1/events",
        "members": "http://example.com/api/v4/projects/1/members"
      }
    }

Unarchive a project
-------------------

Unarchives the project if the user is either admin or the project owner
of this project. This action is idempotent, thus unarchiving an
non-archived project will not change the project.

::

    POST /projects/:id/unarchive

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" "https://gitlab.example.com/api/v4/projects/5/unarchive"

Example response:

.. code:: json

    {
      "id": 3,
      "description": null,
      "default_branch": "master",
      "visibility": "private",
      "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site",
      "tag_list": [
        "example",
        "disapora project"
      ],
      "owner": {
        "id": 3,
        "name": "Diaspora",
        "created_at": "2013-09-30T13:46:02Z"
      },
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "issues_enabled": true,
      "open_issues_count": 1,
      "merge_requests_enabled": true,
      "jobs_enabled": true,
      "wiki_enabled": true,
      "snippets_enabled": false,
      "resolve_outdated_diff_discussions": false,
      "container_registry_enabled": false,
      "created_at": "2013-09-30T13:46:02Z",
      "last_activity_at": "2013-09-30T13:46:02Z",
      "creator_id": 3,
      "namespace": {
        "id": 3,
        "name": "Diaspora",
        "path": "diaspora",
        "kind": "group",
        "full_path": "diaspora"
      },
      "import_status": "none",
      "import_error": null,
      "permissions": {
        "project_access": {
          "access_level": 10,
          "notification_level": 3
        },
        "group_access": {
          "access_level": 50,
          "notification_level": 3
        }
      },
      "archived": false,
      "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
      "shared_runners_enabled": true,
      "forks_count": 0,
      "star_count": 0,
      "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
      "public_jobs": true,
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "request_access_enabled": false,
      "_links": {
        "self": "http://example.com/api/v4/projects",
        "issues": "http://example.com/api/v4/projects/1/issues",
        "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
        "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
        "labels": "http://example.com/api/v4/projects/1/labels",
        "events": "http://example.com/api/v4/projects/1/events",
        "members": "http://example.com/api/v4/projects/1/members"
      }
    }

Remove project
--------------

Removes a project including all associated resources (issues, merge
requests etc.)

::

    DELETE /projects/:id

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

Upload a file
-------------

Uploads a file to the specified project to be used in an issue or merge
request description, or a comment.

::

    POST /projects/:id/uploads

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``file``    | string           | yes        | The file to be uploaded                                                              |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

To upload a file from your filesystem, use the ``--form`` argument. This
causes cURL to post data using the header
``Content-Type: multipart/form-data``. The ``file=`` parameter must
point to a file on your filesystem and be preceded by ``@``. For
example:

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" --form "file=@dk.png" https://gitlab.example.com/api/v3/projects/5/uploads

Returned object:

.. code:: json

    {
      "alt": "dk",
      "url": "/uploads/66dbcd21ec5d24ed6ea225176098d52b/dk.png",
      "markdown": "![dk](/uploads/66dbcd21ec5d24ed6ea225176098d52b/dk.png)"
    }

    **Note**: The returned ``url`` is relative to the project path. In
    Markdown contexts, the link is automatically expanded when the
    format in ``markdown`` is used.

Share project with group
------------------------

Allow to share project with group.

::

    POST /projects/:id/share

+--------------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute          | Type             | Required   | Description                                                                          |
+====================+==================+============+======================================================================================+
| ``id``             | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+--------------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``group_id``       | integer          | yes        | The ID of the group to share with                                                    |
+--------------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``group_access``   | integer          | yes        | The permissions level to grant the group                                             |
+--------------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``expires_at``     | string           | no         | Share expiration date in ISO 8601 format: 2016-09-26                                 |
+--------------------+------------------+------------+--------------------------------------------------------------------------------------+

Delete a shared project link within a group
-------------------------------------------

Unshare the project from the group. Returns ``204`` and no content on
success.

::

    DELETE /projects/:id/share/:group_id

+----------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute      | Type             | Required   | Description                                                                          |
+================+==================+============+======================================================================================+
| ``id``         | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+----------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``group_id``   | integer          | yes        | The ID of the group                                                                  |
+----------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: bash

    curl --request DELETE --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects/5/share/17

Hooks
-----

Also called Project Hooks and Webhooks. These are different for `System
Hooks <system_hooks.md>`__ that are system wide.

List project hooks
~~~~~~~~~~~~~~~~~~

Get a list of project hooks.

::

    GET /projects/:id/hooks

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

Get project hook
~~~~~~~~~~~~~~~~

Get a specific hook for a project.

::

    GET /projects/:id/hooks/:hook_id

+---------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute     | Type             | Required   | Description                                                                          |
+===============+==================+============+======================================================================================+
| ``id``        | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+---------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``hook_id``   | integer          | yes        | The ID of a project hook                                                             |
+---------------+------------------+------------+--------------------------------------------------------------------------------------+

.. code:: json

    {
      "id": 1,
      "url": "http://example.com/hook",
      "project_id": 3,
      "push_events": true,
      "issues_events": true,
      "merge_requests_events": true,
      "tag_push_events": true,
      "note_events": true,
      "job_events": true,
      "pipeline_events": true,
      "wiki_page_events": true,
      "enable_ssl_verification": true,
      "created_at": "2012-10-12T17:04:47Z"
    }

Add project hook
~~~~~~~~~~~~~~~~

Adds a hook to a specified project.

::

    POST /projects/:id/hooks

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
+------------+-------+-----------+--------------+
| ``url``    | strin | yes       | The hook URL |
|            | g     |           |              |
+------------+-------+-----------+--------------+
| ``push_eve | boole | no        | Trigger hook |
| nts``      | an    |           | on push      |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``issues_e | boole | no        | Trigger hook |
| vents``    | an    |           | on issues    |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``merge_re | boole | no        | Trigger hook |
| quests_eve | an    |           | on merge     |
| nts``      |       |           | requests     |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``tag_push | boole | no        | Trigger hook |
| _events``  | an    |           | on tag push  |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``note_eve | boole | no        | Trigger hook |
| nts``      | an    |           | on note      |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``job_even | boole | no        | Trigger hook |
| ts``       | an    |           | on job       |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``pipeline | boole | no        | Trigger hook |
| _events``  | an    |           | on pipeline  |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``wiki_eve | boole | no        | Trigger hook |
| nts``      | an    |           | on wiki      |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``enable_s | boole | no        | Do SSL       |
| sl_verific | an    |           | verification |
| ation``    |       |           | when         |
|            |       |           | triggering   |
|            |       |           | the hook     |
+------------+-------+-----------+--------------+
| ``token``  | strin | no        | Secret token |
|            | g     |           | to validate  |
|            |       |           | received     |
|            |       |           | payloads;    |
|            |       |           | this will    |
|            |       |           | not be       |
|            |       |           | returned in  |
|            |       |           | the response |
+------------+-------+-----------+--------------+

Edit project hook
~~~~~~~~~~~~~~~~~

Edits a hook for a specified project.

::

    PUT /projects/:id/hooks/:hook_id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
+------------+-------+-----------+--------------+
| ``hook_id` | integ | yes       | The ID of    |
| `          | er    |           | the project  |
|            |       |           | hook         |
+------------+-------+-----------+--------------+
| ``url``    | strin | yes       | The hook URL |
|            | g     |           |              |
+------------+-------+-----------+--------------+
| ``push_eve | boole | no        | Trigger hook |
| nts``      | an    |           | on push      |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``issues_e | boole | no        | Trigger hook |
| vents``    | an    |           | on issues    |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``merge_re | boole | no        | Trigger hook |
| quests_eve | an    |           | on merge     |
| nts``      |       |           | requests     |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``tag_push | boole | no        | Trigger hook |
| _events``  | an    |           | on tag push  |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``note_eve | boole | no        | Trigger hook |
| nts``      | an    |           | on note      |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``job_even | boole | no        | Trigger hook |
| ts``       | an    |           | on job       |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``pipeline | boole | no        | Trigger hook |
| _events``  | an    |           | on pipeline  |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``wiki_eve | boole | no        | Trigger hook |
| nts``      | an    |           | on wiki      |
|            |       |           | events       |
+------------+-------+-----------+--------------+
| ``enable_s | boole | no        | Do SSL       |
| sl_verific | an    |           | verification |
| ation``    |       |           | when         |
|            |       |           | triggering   |
|            |       |           | the hook     |
+------------+-------+-----------+--------------+
| ``token``  | strin | no        | Secret token |
|            | g     |           | to validate  |
|            |       |           | received     |
|            |       |           | payloads;    |
|            |       |           | this will    |
|            |       |           | not be       |
|            |       |           | returned in  |
|            |       |           | the response |
+------------+-------+-----------+--------------+

Delete project hook
~~~~~~~~~~~~~~~~~~~

Removes a hook from a project. This is an idempotent method and can be
called multiple times. Either the hook is available or not.

::

    DELETE /projects/:id/hooks/:hook_id

+---------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute     | Type             | Required   | Description                                                                          |
+===============+==================+============+======================================================================================+
| ``id``        | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+---------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``hook_id``   | integer          | yes        | The ID of the project hook                                                           |
+---------------+------------------+------------+--------------------------------------------------------------------------------------+

Note the JSON response differs if the hook is available or not. If the
project hook is available before it is returned in the JSON response or
an empty response is returned.

Admin fork relation
-------------------

Allows modification of the forked relationship between existing
projects. Available only for admins.

Create a forked from/to relation between existing projects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    POST /projects/:id/fork/:forked_from_id

+----------------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute            | Type             | Required   | Description                                                                          |
+======================+==================+============+======================================================================================+
| ``id``               | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+----------------------+------------------+------------+--------------------------------------------------------------------------------------+
| ``forked_from_id``   | ID               | yes        | The ID of the project that was forked from                                           |
+----------------------+------------------+------------+--------------------------------------------------------------------------------------+

Delete an existing forked from relationship
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    DELETE /projects/:id/fork

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

Search for projects by name
---------------------------

Search for projects by name which are accessible to the authenticated
user. This endpoint can be accessed without authentication if the
project is publicly accessible.

::

    GET /projects

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``search`` | strin | yes       | A string     |
|            | g     |           | contained in |
|            |       |           | the project  |
|            |       |           | name         |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Return       |
| ``         | g     |           | requests     |
|            |       |           | ordered by   |
|            |       |           | ``id``,      |
|            |       |           | ``name``,    |
|            |       |           | ``created_at |
|            |       |           | ``           |
|            |       |           | or           |
|            |       |           | ``last_activ |
|            |       |           | ity_at``     |
|            |       |           | fields       |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Return       |
|            | g     |           | requests     |
|            |       |           | sorted in    |
|            |       |           | ``asc`` or   |
|            |       |           | ``desc``     |
|            |       |           | order        |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects?search=test

Start the Housekeeping task for a Project
-----------------------------------------

    Introduced in GitLab 9.0.

::

    POST /projects/:id/housekeeping

+-------------+------------------+------------+--------------------------------------------------------------------------------------+
| Attribute   | Type             | Required   | Description                                                                          |
+=============+==================+============+======================================================================================+
| ``id``      | integer/string   | yes        | The ID or `URL-encoded path of the project <README.md#namespaced-path-encoding>`__   |
+-------------+------------------+------------+--------------------------------------------------------------------------------------+

Branches
--------

Read more in the `Branches <branches.md>`__ documentation.

Project members
---------------

Read more in the `Project members <members.md>`__ documentation.
