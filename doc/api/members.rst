Group and project members API
=============================

**Valid access levels**

The access levels are defined in the ``Gitlab::Access`` module.
Currently, these levels are recognized:

::

    10 => Guest access
    20 => Reporter access
    30 => Developer access
    40 => Master access
    50 => Owner access # Only valid for groups

List all members of a group or project
--------------------------------------

Gets a list of group or project members viewable by the authenticated
user.

::

    GET /groups/:id/members
    GET /projects/:id/members

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project or   |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``query``  | strin | no        | A query      |
|            | g     |           | string to    |
|            |       |           | search for   |
|            |       |           | members      |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/groups/:id/members
    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects/:id/members

Example response:

.. code:: json

    [
      {
        "id": 1,
        "username": "raymond_smith",
        "name": "Raymond Smith",
        "state": "active",
        "created_at": "2012-10-22T14:13:35Z",
        "access_level": 30
      },
      {
        "id": 2,
        "username": "john_doe",
        "name": "John Doe",
        "state": "active",
        "created_at": "2012-10-22T14:13:35Z",
        "access_level": 30
      }
    ]

Get a member of a group or project
----------------------------------

Gets a member of a group or project.

::

    GET /groups/:id/members/:user_id
    GET /projects/:id/members/:user_id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project or   |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``user_id` | integ | yes       | The user ID  |
| `          | er    |           | of the       |
|            |       |           | member       |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/groups/:id/members/:user_id
    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects/:id/members/:user_id

Example response:

.. code:: json

    {
      "id": 1,
      "username": "raymond_smith",
      "name": "Raymond Smith",
      "state": "active",
      "created_at": "2012-10-22T14:13:35Z",
      "access_level": 30,
      "expires_at": null
    }

Add a member to a group or project
----------------------------------

Adds a member to a group or project.

::

    POST /groups/:id/members
    POST /projects/:id/members

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project or   |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``user_id` | integ | yes       | The user ID  |
| `          | er    |           | of the new   |
|            |       |           | member       |
+------------+-------+-----------+--------------+
| ``access_l | integ | yes       | A valid      |
| evel``     | er    |           | access level |
+------------+-------+-----------+--------------+
| ``expires_ | strin | no        | A date       |
| at``       | g     |           | string in    |
|            |       |           | the format   |
|            |       |           | YEAR-MONTH-D |
|            |       |           | AY           |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" --data "user_id=1&access_level=30" https://gitlab.example.com/api/v4/groups/:id/members
    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" --data "user_id=1&access_level=30" https://gitlab.example.com/api/v4/projects/:id/members

Example response:

.. code:: json

    {
      "id": 1,
      "username": "raymond_smith",
      "name": "Raymond Smith",
      "state": "active",
      "created_at": "2012-10-22T14:13:35Z",
      "access_level": 30
    }

Edit a member of a group or project
-----------------------------------

Updates a member of a group or project.

::

    PUT /groups/:id/members/:user_id
    PUT /projects/:id/members/:user_id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project or   |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``user_id` | integ | yes       | The user ID  |
| `          | er    |           | of the       |
|            |       |           | member       |
+------------+-------+-----------+--------------+
| ``access_l | integ | yes       | A valid      |
| evel``     | er    |           | access level |
+------------+-------+-----------+--------------+
| ``expires_ | strin | no        | A date       |
| at``       | g     |           | string in    |
|            |       |           | the format   |
|            |       |           | YEAR-MONTH-D |
|            |       |           | AY           |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --request PUT --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/groups/:id/members/:user_id?access_level=40
    curl --request PUT --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects/:id/members/:user_id?access_level=40

Example response:

.. code:: json

    {
      "id": 1,
      "username": "raymond_smith",
      "name": "Raymond Smith",
      "state": "active",
      "created_at": "2012-10-22T14:13:35Z",
      "access_level": 40
    }

Remove a member from a group or project
---------------------------------------

Removes a user from a group or project.

::

    DELETE /groups/:id/members/:user_id
    DELETE /projects/:id/members/:user_id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project or   |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``user_id` | integ | yes       | The user ID  |
| `          | er    |           | of the       |
|            |       |           | member       |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --request DELETE --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/groups/:id/members/:user_id
    curl --request DELETE --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects/:id/members/:user_id
