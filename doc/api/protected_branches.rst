Protected branches API
======================

    **Note:** This feature was introduced in GitLab 9.5

**Valid access levels**

The access levels are defined in the
``ProtectedRefAccess::ALLOWED_ACCESS_LEVELS`` constant. Currently, these
levels are recognized:

::

    0  => No access
    30 => Developer access
    40 => Master access

List protected branches
-----------------------

Gets a list of protected branches from a project.

::

    GET /projects/:id/protected_branches

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" 'https://gitlab.example.com/api/v4/projects/5/protected_branches'

Example response:

.. code:: json

    [
      {
        "name": "master",
        "push_access_levels": [
          {
            "access_level": 40,
            "access_level_description": "Masters"
          }
        ],
        "merge_access_levels": [
          {
            "access_level": 40,
            "access_level_description": "Masters"
          }
        ]
      },
      ...
    ]

Get a single protected branch or wildcard protected branch
----------------------------------------------------------

Gets a single protected branch or wildcard protected branch.

::

    GET /projects/:id/protected_branches/:name

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``name``   | strin | yes       | The name of  |
|            | g     |           | the branch   |
|            |       |           | or wildcard  |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" 'https://gitlab.example.com/api/v4/projects/5/protected_branches/master'

Example response:

.. code:: json

    {
      "name": "master",
      "push_access_levels": [
        {
          "access_level": 40,
          "access_level_description": "Masters"
        }
      ],
      "merge_access_levels": [
        {
          "access_level": 40,
          "access_level_description": "Masters"
        }
      ]
    }

Protect repository branches
---------------------------

Protects a single repository branch or several project repository
branches using a wildcard protected branch.

::

    POST /projects/:id/protected_branches

.. code:: bash

    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" 'https://gitlab.example.com/api/v4/projects/5/protected_branches?name=*-stable&push_access_level=30&merge_access_level=30'

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``name``   | strin | yes       | The name of  |
|            | g     |           | the branch   |
|            |       |           | or wildcard  |
+------------+-------+-----------+--------------+
| ``push_acc | strin | no        | Access       |
| ess_level` | g     |           | levels       |
| `          |       |           | allowed to   |
|            |       |           | push         |
|            |       |           | (defaults:   |
|            |       |           | ``40``,      |
|            |       |           | master       |
|            |       |           | access       |
|            |       |           | level)       |
+------------+-------+-----------+--------------+
| ``merge_ac | strin | no        | Access       |
| cess_level | g     |           | levels       |
| ``         |       |           | allowed to   |
|            |       |           | merge        |
|            |       |           | (defaults:   |
|            |       |           | ``40``,      |
|            |       |           | master       |
|            |       |           | access       |
|            |       |           | level)       |
+------------+-------+-----------+--------------+

Example response:

.. code:: json

    {
      "name": "*-stable",
      "push_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Masters"
        }
      ],
      "merge_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Masters"
        }
      ]
    }

Unprotect repository branches
-----------------------------

Unprotects the given protected branch or wildcard protected branch.

::

    DELETE /projects/:id/protected_branches/:name

.. code:: bash

    curl --request DELETE --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" 'https://gitlab.example.com/api/v4/projects/5/protected_branches/*-stable'

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``name``   | strin | yes       | The name of  |
|            | g     |           | the branch   |
+------------+-------+-----------+--------------+
