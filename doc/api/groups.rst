Groups API
==========

List groups
-----------

Get a list of visible groups for the authenticated user. When accessed
without authentication, only public groups are returned.

Parameters:

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``skip_gro | array | no        | Skip the     |
| ups``      | of    |           | group IDs    |
|            | integ |           | passed       |
|            | ers   |           |              |
+------------+-------+-----------+--------------+
| ``all_avai | boole | no        | Show all the |
| lable``    | an    |           | groups you   |
|            |       |           | have access  |
|            |       |           | to (defaults |
|            |       |           | to ``false`` |
|            |       |           | for          |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | users)       |
+------------+-------+-----------+--------------+
| ``search`` | strin | no        | Return the   |
|            | g     |           | list of      |
|            |       |           | authorized   |
|            |       |           | groups       |
|            |       |           | matching the |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Order groups |
| ``         | g     |           | by ``name``  |
|            |       |           | or ``path``. |
|            |       |           | Default is   |
|            |       |           | ``name``     |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Order groups |
|            | g     |           | in ``asc``   |
|            |       |           | or ``desc``  |
|            |       |           | order.       |
|            |       |           | Default is   |
|            |       |           | ``asc``      |
+------------+-------+-----------+--------------+
| ``statisti | boole | no        | Include      |
| cs``       | an    |           | group        |
|            |       |           | statistics   |
|            |       |           | (admins      |
|            |       |           | only)        |
+------------+-------+-----------+--------------+
| ``owned``  | boole | no        | Limit to     |
|            | an    |           | groups owned |
|            |       |           | by the       |
|            |       |           | current user |
+------------+-------+-----------+--------------+

::

    GET /groups

.. code:: json

    [
      {
        "id": 1,
        "name": "Foobar Group",
        "path": "foo-bar",
        "description": "An interesting group",
        "visibility": "public",
        "lfs_enabled": true,
        "avatar_url": "http://localhost:3000/uploads/group/avatar/1/foo.jpg",
        "web_url": "http://localhost:3000/groups/foo-bar",
        "request_access_enabled": false,
        "full_name": "Foobar Group",
        "full_path": "foo-bar",
        "parent_id": null
      }
    ]

When adding the parameter ``statistics=true`` and the authenticated user
is an admin, additional group statistics are returned.

::

    GET /groups?statistics=true

.. code:: json

    [
      {
        "id": 1,
        "name": "Foobar Group",
        "path": "foo-bar",
        "description": "An interesting group",
        "visibility": "public",
        "lfs_enabled": true,
        "avatar_url": "http://localhost:3000/uploads/group/avatar/1/foo.jpg",
        "web_url": "http://localhost:3000/groups/foo-bar",
        "request_access_enabled": false,
        "full_name": "Foobar Group",
        "full_path": "foo-bar",
        "parent_id": null,
        "statistics": {
          "storage_size" : 212,
          "repository_size" : 33,
          "lfs_objects_size" : 123,
          "job_artifacts_size" : 57

        }
      }
    ]

You can search for groups by name or path, see below.

You can filter by `custom attributes <custom_attributes.md>`__ with:

::

    GET /groups?custom_attributes[key]=value&custom_attributes[other_key]=other_value

List a groups's subgroups
-------------------------

    `Introduced <https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15142>`__
    in GitLab 10.3.

Get a list of visible direct subgroups in this group. When accessed
without authentication, only public groups are returned.

Parameters:

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | of the       |
|            |       |           | parent group |
+------------+-------+-----------+--------------+
| ``skip_gro | array | no        | Skip the     |
| ups``      | of    |           | group IDs    |
|            | integ |           | passed       |
|            | ers   |           |              |
+------------+-------+-----------+--------------+
| ``all_avai | boole | no        | Show all the |
| lable``    | an    |           | groups you   |
|            |       |           | have access  |
|            |       |           | to (defaults |
|            |       |           | to ``false`` |
|            |       |           | for          |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | users)       |
+------------+-------+-----------+--------------+
| ``search`` | strin | no        | Return the   |
|            | g     |           | list of      |
|            |       |           | authorized   |
|            |       |           | groups       |
|            |       |           | matching the |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Order groups |
| ``         | g     |           | by ``name``  |
|            |       |           | or ``path``. |
|            |       |           | Default is   |
|            |       |           | ``name``     |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Order groups |
|            | g     |           | in ``asc``   |
|            |       |           | or ``desc``  |
|            |       |           | order.       |
|            |       |           | Default is   |
|            |       |           | ``asc``      |
+------------+-------+-----------+--------------+
| ``statisti | boole | no        | Include      |
| cs``       | an    |           | group        |
|            |       |           | statistics   |
|            |       |           | (admins      |
|            |       |           | only)        |
+------------+-------+-----------+--------------+
| ``owned``  | boole | no        | Limit to     |
|            | an    |           | groups owned |
|            |       |           | by the       |
|            |       |           | current user |
+------------+-------+-----------+--------------+

::

    GET /groups/:id/subgroups

.. code:: json

    [
      {
        "id": 1,
        "name": "Foobar Group",
        "path": "foo-bar",
        "description": "An interesting group",
        "visibility": "public",
        "lfs_enabled": true,
        "avatar_url": "http://gitlab.example.com/uploads/group/avatar/1/foo.jpg",
        "web_url": "http://gitlab.example.com/groups/foo-bar",
        "request_access_enabled": false,
        "full_name": "Foobar Group",
        "full_path": "foo-bar",
        "parent_id": 123
      }
    ]

List a group's projects
-----------------------

Get a list of projects in this group. When accessed without
authentication, only public projects are returned.

::

    GET /groups/:id/projects

Parameters:

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``archived | boole | no        | Limit by     |
| ``         | an    |           | archived     |
|            |       |           | status       |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | Limit by     |
| ty``       | g     |           | visibility   |
|            |       |           | ``public``,  |
|            |       |           | ``internal`` |
|            |       |           | ,            |
|            |       |           | or           |
|            |       |           | ``private``  |
+------------+-------+-----------+--------------+
| ``order_by | strin | no        | Return       |
| ``         | g     |           | projects     |
|            |       |           | ordered by   |
|            |       |           | ``id``,      |
|            |       |           | ``name``,    |
|            |       |           | ``path``,    |
|            |       |           | ``created_at |
|            |       |           | ``,          |
|            |       |           | ``updated_at |
|            |       |           | ``,          |
|            |       |           | or           |
|            |       |           | ``last_activ |
|            |       |           | ity_at``     |
|            |       |           | fields.      |
|            |       |           | Default is   |
|            |       |           | ``created_at |
|            |       |           | ``           |
+------------+-------+-----------+--------------+
| ``sort``   | strin | no        | Return       |
|            | g     |           | projects     |
|            |       |           | sorted in    |
|            |       |           | ``asc`` or   |
|            |       |           | ``desc``     |
|            |       |           | order.       |
|            |       |           | Default is   |
|            |       |           | ``desc``     |
+------------+-------+-----------+--------------+
| ``search`` | strin | no        | Return list  |
|            | g     |           | of           |
|            |       |           | authorized   |
|            |       |           | projects     |
|            |       |           | matching the |
|            |       |           | search       |
|            |       |           | criteria     |
+------------+-------+-----------+--------------+
| ``simple`` | boole | no        | Return only  |
|            | an    |           | the ID, URL, |
|            |       |           | name, and    |
|            |       |           | path of each |
|            |       |           | project      |
+------------+-------+-----------+--------------+
| ``owned``  | boole | no        | Limit by     |
|            | an    |           | projects     |
|            |       |           | owned by the |
|            |       |           | current user |
+------------+-------+-----------+--------------+
| ``starred` | boole | no        | Limit by     |
| `          | an    |           | projects     |
|            |       |           | starred by   |
|            |       |           | the current  |
|            |       |           | user         |
+------------+-------+-----------+--------------+

Example response:

.. code:: json

    [
      {
        "id": 9,
        "description": "foo",
        "default_branch": "master",
        "tag_list": [],
        "archived": false,
        "visibility": "internal",
        "ssh_url_to_repo": "git@gitlab.example.com/html5-boilerplate.git",
        "http_url_to_repo": "http://gitlab.example.com/h5bp/html5-boilerplate.git",
        "web_url": "http://gitlab.example.com/h5bp/html5-boilerplate",
        "name": "Html5 Boilerplate",
        "name_with_namespace": "Experimental / Html5 Boilerplate",
        "path": "html5-boilerplate",
        "path_with_namespace": "h5bp/html5-boilerplate",
        "issues_enabled": true,
        "merge_requests_enabled": true,
        "wiki_enabled": true,
        "jobs_enabled": true,
        "snippets_enabled": true,
        "created_at": "2016-04-05T21:40:50.169Z",
        "last_activity_at": "2016-04-06T16:52:08.432Z",
        "shared_runners_enabled": true,
        "creator_id": 1,
        "namespace": {
          "id": 5,
          "name": "Experimental",
          "path": "h5bp",
          "kind": "group"
        },
        "avatar_url": null,
        "star_count": 1,
        "forks_count": 0,
        "open_issues_count": 3,
        "public_jobs": true,
        "shared_with_groups": [],
        "request_access_enabled": false
      }
    ]

Details of a group
------------------

Get all details of a group. This endpoint can be accessed without
authentication if the group is publicly accessible.

::

    GET /groups/:id

Parameters:

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/groups/4

Example response:

.. code:: json

    {
      "id": 4,
      "name": "Twitter",
      "path": "twitter",
      "description": "Aliquid qui quis dignissimos distinctio ut commodi voluptas est.",
      "visibility": "public",
      "avatar_url": null,
      "web_url": "https://gitlab.example.com/groups/twitter",
      "request_access_enabled": false,
      "full_name": "Twitter",
      "full_path": "twitter",
      "parent_id": null,
      "projects": [
        {
          "id": 7,
          "description": "Voluptas veniam qui et beatae voluptas doloremque explicabo facilis.",
          "default_branch": "master",
          "tag_list": [],
          "archived": false,
          "visibility": "public",
          "ssh_url_to_repo": "git@gitlab.example.com:twitter/typeahead-js.git",
          "http_url_to_repo": "https://gitlab.example.com/twitter/typeahead-js.git",
          "web_url": "https://gitlab.example.com/twitter/typeahead-js",
          "name": "Typeahead.Js",
          "name_with_namespace": "Twitter / Typeahead.Js",
          "path": "typeahead-js",
          "path_with_namespace": "twitter/typeahead-js",
          "issues_enabled": true,
          "merge_requests_enabled": true,
          "wiki_enabled": true,
          "jobs_enabled": true,
          "snippets_enabled": false,
          "container_registry_enabled": true,
          "created_at": "2016-06-17T07:47:25.578Z",
          "last_activity_at": "2016-06-17T07:47:25.881Z",
          "shared_runners_enabled": true,
          "creator_id": 1,
          "namespace": {
            "id": 4,
            "name": "Twitter",
            "path": "twitter",
            "kind": "group"
          },
          "avatar_url": null,
          "star_count": 0,
          "forks_count": 0,
          "open_issues_count": 3,
          "public_jobs": true,
          "shared_with_groups": [],
          "request_access_enabled": false
        },
        {
          "id": 6,
          "description": "Aspernatur omnis repudiandae qui voluptatibus eaque.",
          "default_branch": "master",
          "tag_list": [],
          "archived": false,
          "visibility": "internal",
          "ssh_url_to_repo": "git@gitlab.example.com:twitter/flight.git",
          "http_url_to_repo": "https://gitlab.example.com/twitter/flight.git",
          "web_url": "https://gitlab.example.com/twitter/flight",
          "name": "Flight",
          "name_with_namespace": "Twitter / Flight",
          "path": "flight",
          "path_with_namespace": "twitter/flight",
          "issues_enabled": true,
          "merge_requests_enabled": true,
          "wiki_enabled": true,
          "jobs_enabled": true,
          "snippets_enabled": false,
          "container_registry_enabled": true,
          "created_at": "2016-06-17T07:47:24.661Z",
          "last_activity_at": "2016-06-17T07:47:24.838Z",
          "shared_runners_enabled": true,
          "creator_id": 1,
          "namespace": {
            "id": 4,
            "name": "Twitter",
            "path": "twitter",
            "kind": "group"
          },
          "avatar_url": null,
          "star_count": 0,
          "forks_count": 0,
          "open_issues_count": 8,
          "public_jobs": true,
          "shared_with_groups": [],
          "request_access_enabled": false
        }
      ],
      "shared_projects": [
        {
          "id": 8,
          "description": "Velit eveniet provident fugiat saepe eligendi autem.",
          "default_branch": "master",
          "tag_list": [],
          "archived": false,
          "visibility": "private",
          "ssh_url_to_repo": "git@gitlab.example.com:h5bp/html5-boilerplate.git",
          "http_url_to_repo": "https://gitlab.example.com/h5bp/html5-boilerplate.git",
          "web_url": "https://gitlab.example.com/h5bp/html5-boilerplate",
          "name": "Html5 Boilerplate",
          "name_with_namespace": "H5bp / Html5 Boilerplate",
          "path": "html5-boilerplate",
          "path_with_namespace": "h5bp/html5-boilerplate",
          "issues_enabled": true,
          "merge_requests_enabled": true,
          "wiki_enabled": true,
          "jobs_enabled": true,
          "snippets_enabled": false,
          "container_registry_enabled": true,
          "created_at": "2016-06-17T07:47:27.089Z",
          "last_activity_at": "2016-06-17T07:47:27.310Z",
          "shared_runners_enabled": true,
          "creator_id": 1,
          "namespace": {
            "id": 5,
            "name": "H5bp",
            "path": "h5bp",
            "kind": "group"
          },
          "avatar_url": null,
          "star_count": 0,
          "forks_count": 0,
          "open_issues_count": 4,
          "public_jobs": true,
          "shared_with_groups": [
            {
              "group_id": 4,
              "group_name": "Twitter",
              "group_access_level": 30
            },
            {
              "group_id": 3,
              "group_name": "Gitlab Org",
              "group_access_level": 10
            }
          ]
        }
      ]
    }

New group
---------

Creates a new project group. Available only for users who can create
groups.

::

    POST /groups

Parameters:

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``name``   | strin | yes       | The name of  |
|            | g     |           | the group    |
+------------+-------+-----------+--------------+
| ``path``   | strin | yes       | The path of  |
|            | g     |           | the group    |
+------------+-------+-----------+--------------+
| ``descript | strin | no        | The group's  |
| ion``      | g     |           | description  |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | The group's  |
| ty``       | g     |           | visibility.  |
|            |       |           | Can be       |
|            |       |           | ``private``, |
|            |       |           | ``internal`` |
|            |       |           | ,            |
|            |       |           | or           |
|            |       |           | ``public``.  |
+------------+-------+-----------+--------------+
| ``lfs_enab | boole | no        | Enable/disab |
| led``      | an    |           | le           |
|            |       |           | Large File   |
|            |       |           | Storage      |
|            |       |           | (LFS) for    |
|            |       |           | the projects |
|            |       |           | in this      |
|            |       |           | group        |
+------------+-------+-----------+--------------+
| ``request_ | boole | no        | Allow users  |
| access_ena | an    |           | to request   |
| bled``     |       |           | member       |
|            |       |           | access.      |
+------------+-------+-----------+--------------+
| ``parent_i | integ | no        | The parent   |
| d``        | er    |           | group id for |
|            |       |           | creating     |
|            |       |           | nested       |
|            |       |           | group.       |
+------------+-------+-----------+--------------+

Transfer project to group
-------------------------

Transfer a project to the Group namespace. Available only for admin

::

    POST  /groups/:id/projects/:project_id

Parameters:

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID or    |
|            | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | group <READM |
|            |       |           | E.md#namespa |
|            |       |           | ced-path-enc |
|            |       |           | oding>`__    |
|            |       |           | owned by the |
|            |       |           | authenticate |
|            |       |           | d            |
|            |       |           | user         |
+------------+-------+-----------+--------------+
| ``project_ | integ | yes       | The ID or    |
| id``       | er/st |           | `URL-encoded |
|            | ring  |           | path of the  |
|            |       |           | project <REA |
|            |       |           | DME.md#names |
|            |       |           | paced-path-e |
|            |       |           | ncoding>`__  |
+------------+-------+-----------+--------------+

Update group
------------

Updates the project group. Only available to group owners and
administrators.

::

    PUT /groups/:id

+------------+-------+-----------+--------------+
| Attribute  | Type  | Required  | Description  |
+============+=======+===========+==============+
| ``id``     | integ | yes       | The ID of    |
|            | er    |           | the group    |
+------------+-------+-----------+--------------+
| ``name``   | strin | no        | The name of  |
|            | g     |           | the group    |
+------------+-------+-----------+--------------+
| ``path``   | strin | no        | The path of  |
|            | g     |           | the group    |
+------------+-------+-----------+--------------+
| ``descript | strin | no        | The          |
| ion``      | g     |           | description  |
|            |       |           | of the group |
+------------+-------+-----------+--------------+
| ``visibili | strin | no        | The          |
| ty``       | g     |           | visibility   |
|            |       |           | level of the |
|            |       |           | group. Can   |
|            |       |           | be           |
|            |       |           | ``private``, |
|            |       |           | ``internal`` |
|            |       |           | ,            |
|            |       |           | or           |
|            |       |           | ``public``.  |
+------------+-------+-----------+--------------+
| ``lfs_enab | boole | no        | Enable/disab |
| led``      | an    |           | le           |
| (optional) |       |           | Large File   |
|            |       |           | Storage      |
|            |       |           | (LFS) for    |
|            |       |           | the projects |
|            |       |           | in this      |
|            |       |           | group        |
+------------+-------+-----------+--------------+
| ``request_ | boole | no        | Allow users  |
| access_ena | an    |           | to request   |
| bled``     |       |           | member       |
|            |       |           | access.      |
+------------+-------+-----------+--------------+

.. code:: bash

    curl --request PUT --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" "https://gitlab.example.com/api/v4/groups/5?name=Experimental"

Example response:

.. code:: json

    {
      "id": 5,
      "name": "Experimental",
      "path": "h5bp",
      "description": "foo",
      "visibility": "internal",
      "avatar_url": null,
      "web_url": "http://gitlab.example.com/groups/h5bp",
      "request_access_enabled": false,
      "full_name": "Foobar Group",
      "full_path": "foo-bar",
      "parent_id": null,
      "projects": [
        {
          "id": 9,
          "description": "foo",
          "default_branch": "master",
          "tag_list": [],
          "public": false,
          "archived": false,
          "visibility": "internal",
          "ssh_url_to_repo": "git@gitlab.example.com/html5-boilerplate.git",
          "http_url_to_repo": "http://gitlab.example.com/h5bp/html5-boilerplate.git",
          "web_url": "http://gitlab.example.com/h5bp/html5-boilerplate",
          "name": "Html5 Boilerplate",
          "name_with_namespace": "Experimental / Html5 Boilerplate",
          "path": "html5-boilerplate",
          "path_with_namespace": "h5bp/html5-boilerplate",
          "issues_enabled": true,
          "merge_requests_enabled": true,
          "wiki_enabled": true,
          "jobs_enabled": true,
          "snippets_enabled": true,
          "created_at": "2016-04-05T21:40:50.169Z",
          "last_activity_at": "2016-04-06T16:52:08.432Z",
          "shared_runners_enabled": true,
          "creator_id": 1,
          "namespace": {
            "id": 5,
            "name": "Experimental",
            "path": "h5bp",
            "kind": "group"
          },
          "avatar_url": null,
          "star_count": 1,
          "forks_count": 0,
          "open_issues_count": 3,
          "public_jobs": true,
          "shared_with_groups": [],
          "request_access_enabled": false
        }
      ]
    }

Remove group
------------

Removes group with all projects inside.

::

    DELETE /groups/:id

Parameters:

-  ``id`` (required) - The ID or path of a user group

Search for group
----------------

Get all groups that match your string in their name or path.

::

    GET /groups?search=foobar

.. code:: json

    [
      {
        "id": 1,
        "name": "Foobar Group",
        "path": "foo-bar",
        "description": "An interesting group"
      }
    ]

Group members
-------------

Please consult the `Group Members <members.md>`__ documentation.

Namespaces in groups
--------------------

By default, groups only get 20 namespaces at a time because the API
results are paginated.

To get more (up to 100), pass the following as an argument to the API
call:

::

    /groups?per_page=100

And to switch pages add:

::

    /groups?per_page=100&page=2
