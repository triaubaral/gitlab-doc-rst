GitLab basics
=============

Step-by-step guides on the basics of working with Git and GitLab.

-  `Command line basics <command-line-commands.md>`__
-  `Start using Git on the command line <start-using-git.md>`__
-  `Create and add your SSH Keys <create-your-ssh-keys.md>`__
-  `Create a project <create-project.md>`__
-  `Create a group <../user/group/index.md#create-a-new-group>`__
-  `Create a branch <create-branch.md>`__
-  `Fork a project <fork-project.md>`__
-  `Add a file <add-file.md>`__
-  `Add an image <add-image.md>`__
-  `Create an issue <../user/project/issues/create_new_issue.md>`__
-  `Create a merge request <add-merge-request.md>`__
