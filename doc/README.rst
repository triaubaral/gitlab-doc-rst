GitLab Documentation
====================

Welcome to `GitLab <https://about.gitlab.com/>`__, a Git-based fully
featured platform for software development!

GitLab offers the most scalable Git-based fully integrated platform for
software development, with flexible products and subscription plans:

-  **GitLab Community Edition (CE)** is an `opensource
   product <https://gitlab.com/gitlab-org/gitlab-ce/>`__, self-hosted,
   free to use. Every feature available in GitLab CE is also available
   on GitLab Enterprise Edition (Starter and Premium) and GitLab.com.
-  **GitLab Enterprise Edition (EE)** is an `opencore
   product <https://gitlab.com/gitlab-org/gitlab-ee/>`__, self-hosted,
   fully featured solution of GitLab, available under distinct
   `subscriptions <https://about.gitlab.com/products/>`__: **GitLab
   Enterprise Edition Starter (EES)**, **GitLab Enterprise Edition
   Premium (EEP)**, and **GitLab Enterprise Edition Ultimate (EEU)**.
-  **GitLab.com**: SaaS GitLab solution, with `free and paid
   subscriptions <https://about.gitlab.com/gitlab-com/>`__. GitLab.com
   is hosted by GitLab, Inc., and administrated by GitLab (users don't
   have access to admin settings).

    **GitLab EE** contains all features available in **GitLab CE**, plus
    premium features available in each version: **Enterprise Edition
    Starter** (**EES**), **Enterprise Edition Premium** (**EEP**), and
    **Enterprise Edition Ultimate** (**EEU**). Everything available in
    **EES** is also available in **EEP**. Every feature available in
    **EEP** is also available in **EEU**.

--------------

Shortcuts to GitLab's most visited docs:

+--------------------------------------------------------------+--------------------------------------------------+
| `GitLab CI/CD <ci/README.md>`__                              | Other                                            |
+==============================================================+==================================================+
| `Quick start guide <ci/quick_start/README.md>`__             | `API <api/README.md>`__                          |
+--------------------------------------------------------------+--------------------------------------------------+
| `Configuring ``.gitlab-ci.yml`` <ci/yaml/README.md>`__       | `SSH authentication <ssh/README.md>`__           |
+--------------------------------------------------------------+--------------------------------------------------+
| `Using Docker images <ci/docker/using_docker_images.md>`__   | `GitLab Pages <user/project/pages/index.md>`__   |
+--------------------------------------------------------------+--------------------------------------------------+

-  `User documentation <user/index.md>`__
-  `Administrator documentation <administration/index.md>`__
-  `Contributor documentation <#contributor-documentation>`__

Getting started with GitLab
---------------------------

-  `GitLab Basics <gitlab-basics/README.md>`__: Start working on your
   command line and on GitLab.
-  `GitLab Workflow <workflow/README.md>`__: Enhance your workflow with
   the best of GitLab Workflow.
-  See also `GitLab Workflow - an
   overview <https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/>`__.
-  `GitLab Markdown <user/markdown.md>`__: GitLab's advanced formatting
   system (GitLab Flavored Markdown).
-  `GitLab Quick Actions <user/project/quick_actions.md>`__: Textual
   shortcuts for common actions on issues or merge requests that are
   usually done by clicking buttons or dropdowns in GitLab's UI.
-  `Auto DevOps <topics/autodevops/index.md>`__

User account
~~~~~~~~~~~~

-  `User account <user/profile/index.md>`__: Manage your account
-  `Authentication <topics/authentication/index.md>`__: Account security
   with two-factor authentication, setup your ssh keys and deploy keys
   for secure access to your projects.
-  `Profile settings <user/profile/index.md#profile-settings>`__: Manage
   your profile settings, two factor authentication and more.
-  `User permissions <user/permissions.md>`__: Learn what each role in a
   project (external/guest/reporter/developer/master/owner) can do.

Projects and groups
~~~~~~~~~~~~~~~~~~~

-  `Projects <user/project/index.md>`__:
-  `Project settings <user/project/settings/index.md>`__
-  `Create a project <gitlab-basics/create-project.md>`__
-  `Fork a project <gitlab-basics/fork-project.md>`__
-  `Importing and exporting projects between
   instances <user/project/settings/import_export.md>`__.
-  `Project access <public_access/public_access.md>`__: Setting up your
   project's visibility to public, internal, or private.
-  `GitLab Pages <user/project/pages/index.md>`__: Build, test, and
   deploy your static website with GitLab Pages.
-  `Groups <user/group/index.md>`__: Organize your projects in groups.
-  `Subgroups <user/group/subgroups/index.md>`__
-  `Search through GitLab <user/search/index.md>`__: Search for issues,
   merge requests, projects, groups, todos, and issues in Issue Boards.
-  `Snippets <user/snippets.md>`__: Snippets allow you to create little
   bits of code.
-  `Wikis <user/project/wiki/index.md>`__: Enhance your repository
   documentation with built-in wikis.

Repository
~~~~~~~~~~

Manage your `repositories <user/project/repository/index.md>`__ from the
UI (user interface):

-  `Files <user/project/repository/index.md#files>`__
-  `Create a
   file <user/project/repository/web_editor.md#create-a-file>`__
-  `Upload a
   file <user/project/repository/web_editor.md#upload-a-file>`__
-  `File
   templates <user/project/repository/web_editor.md#template-dropdowns>`__
-  `Create a
   directory <user/project/repository/web_editor.md#create-a-directory>`__
-  `Start a merge
   request <user/project/repository/web_editor.md#tips>`__ (when
   committing via UI)
-  `Branches <user/project/repository/branches/index.md>`__
-  `Default
   branch <user/project/repository/branches/index.md#default-branch>`__
-  `Create a
   branch <user/project/repository/web_editor.md#create-a-new-branch>`__
-  `Protected
   branches <user/project/protected_branches.md#protected-branches>`__
-  `Delete merged
   branches <user/project/repository/branches/index.md#delete-merged-branches>`__
-  `Commits <user/project/repository/index.md#commits>`__
-  `Signing
   commits <user/project/repository/gpg_signed_commits/index.md>`__: use
   GPG to sign your commits.

Issues and Merge Requests (MRs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `Discussions <user/discussions/index.md>`__: Threads, comments, and
   resolvable discussions in issues, commits, and merge requests.
-  `Issues <user/project/issues/index.md>`__
-  `Project issue Board <user/project/issue_board.md>`__
-  `Issues and merge requests
   templates <user/project/description_templates.md>`__: Create
   templates for submitting new issues and merge requests.
-  `Labels <user/project/labels.md>`__: Categorize your issues or merge
   requests based on descriptive titles.
-  `Merge Requests <user/project/merge_requests/index.md>`__
-  `Work In Progress Merge
   Requests <user/project/merge_requests/work_in_progress_merge_requests.md>`__
-  `Merge Request discussion
   resolution <user/discussions/index.md#moving-a-single-discussion-to-a-new-issue>`__:
   Resolve discussions, move discussions in a merge request to an issue,
   only allow merge requests to be merged if all discussions are
   resolved.
-  `Checkout merge requests
   locally <user/project/merge_requests/index.md#checkout-merge-requests-locally>`__
-  `Cherry-pick <user/project/merge_requests/cherry_pick_changes.md>`__
-  `Milestones <user/project/milestones/index.md>`__: Organize issues
   and merge requests into a cohesive group, optionally setting a due
   date.
-  `Todos <workflow/todos.md>`__: A chronological list of to-dos that
   are waiting for your input, all in a simple dashboard.

Git and GitLab
~~~~~~~~~~~~~~

-  `Git <topics/git/index.md>`__: Getting started with Git, branching
   strategies, Git LFS, advanced use.
-  `Git
   cheatsheet <https://gitlab.com/gitlab-com/marketing/raw/master/design/print/git-cheatsheet/print-pdf/git-cheatsheet.pdf>`__:
   Download a PDF describing the most used Git operations.
-  `GitLab Flow <workflow/gitlab_flow.md>`__: explore the best of Git
   with the GitLab Flow strategy.

Migrate and import your projects from other platforms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `Importing to GitLab <user/project/import/index.md>`__: Import your
   projects from GitHub, Bitbucket, GitLab.com, FogBugz and SVN into
   GitLab.
-  `Migrating from SVN <workflow/importing/migrating_from_svn.md>`__:
   Convert a SVN repository to Git and GitLab.

Continuous Integration, Delivery, and Deployment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `GitLab CI <ci/README.md>`__: Explore the features and capabilities
   of Continuous Integration, Continuous Delivery, and Continuous
   Deployment with GitLab.
-  `Auto Deploy <ci/autodeploy/index.md>`__: Configure GitLab CI for the
   deployment of your application.
-  `Review Apps <ci/review_apps/index.md>`__: Preview changes to your
   app right from a merge request.
-  `GitLab Cycle Analytics <user/project/cycle_analytics.md>`__: Cycle
   Analytics measures the time it takes to go from an `idea to
   production <https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/#from-idea-to-production-with-gitlab>`__
   for each project you have.
-  `GitLab Container Registry <user/project/container_registry.md>`__:
   Learn how to use GitLab's built-in Container Registry.

Automation
~~~~~~~~~~

-  `API <api/README.md>`__: Automate GitLab via a simple and powerful
   API.
-  `GitLab Webhooks <user/project/integrations/webhooks.md>`__: Let
   GitLab notify you when new code has been pushed to your project.

Integrations
~~~~~~~~~~~~

-  `Project Services <user/project/integrations/project_services.md>`__:
   Integrate a project with external services, such as CI and chat.
-  `GitLab Integration <integration/README.md>`__: Integrate with
   multiple third-party services with GitLab to allow external issue
   trackers and external authentication.
-  `Trello Power-Up <integration/trello_power_up.md>`__: Integrate with
   GitLab's Trello Power-Up

--------------

Administrator documentation
---------------------------

`Administration documentation <administration/index.md>`__ applies to
admin users of GitLab self-hosted instances:

-  GitLab Community Edition
-  GitLab `Enterprise Editions <https://about.gitlab.com/gitlab-ee/>`__
-  Enterprise Edition Starter (EES)
-  Enterprise Edition Premium (EEP)
-  Enterprise Edition Ultimate (EEU)

Learn how to install, configure, update, upgrade, integrate, and
maintain your own instance. Regular users don't have access to GitLab
administration tools and settings.

Contributor documentation
-------------------------

GitLab Community Edition is
`opensource <https://gitlab.com/gitlab-org/gitlab-ce/>`__ and Enterprise
Editions are `opencore <https://gitlab.com/gitlab-org/gitlab-ee/>`__.
Learn how to contribute to GitLab:

-  `Development <development/README.md>`__: All styleguides and
   explanations how to contribute.
-  `Legal <legal/README.md>`__: Contributor license agreements.
-  `Writing documentation <development/writing_documentation.md>`__:
   Contributing to GitLab Docs.
