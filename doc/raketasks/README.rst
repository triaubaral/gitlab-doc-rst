Rake tasks
==========

-  `Backup restore <backup_restore.md>`__
-  `Check <check.md>`__
-  `Cleanup <cleanup.md>`__
-  `Features <features.md>`__
-  `LDAP Maintenance <../administration/raketasks/ldap.md>`__
-  `General Maintenance <maintenance.md>`__ and self-checks
-  `User management <user_management.md>`__
-  `Webhooks <web_hooks.md>`__
-  `Import <import.md>`__ of git repositories in bulk
-  `Rebuild authorized\_keys
   file <http://docs.gitlab.com/ce/raketasks/maintenance.html#rebuild-authorized_keys-file>`__
   task for administrators
