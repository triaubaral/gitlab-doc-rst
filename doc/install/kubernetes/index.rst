Installing GitLab on Kubernetes
===============================

    **Note**: These charts have been tested on Google Kubernetes Engine
    and Azure Container Service. Other Kubernetes installations may work
    as well, if not please `open an
    issue <https://gitlab.com/charts/charts.gitlab.io/issues>`__.

The easiest method to deploy GitLab on
`Kubernetes <https://kubernetes.io/>`__ is to take advantage of GitLab's
Helm charts.
`Helm <https://github.com/kubernetes/helm/blob/master/README.md>`__ is a
package management tool for Kubernetes, allowing apps to be easily
managed via their Charts. A
`Chart <https://github.com/kubernetes/charts>`__ is a detailed
description of the application including how it should be deployed,
upgraded, and configured.

Chart Overview
--------------

-  **`GitLab-Omnibus <gitlab_omnibus.md>`__**: The best way to run
   GitLab on Kubernetes today, suited for small deployments. The chart
   is in beta and will be deprecated by the `cloud native GitLab
   chart <#cloud-native-gitlab-chart>`__.
-  **`Cloud Native GitLab
   Chart <https://gitlab.com/charts/helm.gitlab.io/blob/master/README.md>`__**:
   The next generation GitLab chart, currently in development. Will
   support large deployments with horizontal scaling of individual
   GitLab components.
-  Other Charts
-  `GitLab Runner Chart <gitlab_runner_chart.md>`__: For deploying just
   the GitLab Runner.
-  `Advanced GitLab Installation <gitlab_chart.md>`__: Deprecated, being
   replaced by the `cloud native GitLab
   chart <#cloud-native-gitlab-chart>`__. Provides additional deployment
   options, but provides less functionality out-of-the-box.
-  `Community Contributed Charts <#community-contributed-charts>`__:
   Community contributed charts, deprecated by the official GitLab
   chart.

GitLab-Omnibus Chart (Recommended)
----------------------------------

    **Note**: This chart is in beta while `additional
    features <https://gitlab.com/charts/charts.gitlab.io/issues/68>`__
    are being added.

This chart is the best available way to operate GitLab on Kubernetes. It
deploys and configures nearly all features of GitLab, including: a
`Runner <https://docs.gitlab.com/runner/>`__, `Container
Registry <../../user/project/container_registry.html#gitlab-container-registry>`__,
`Mattermost <https://docs.gitlab.com/omnibus/gitlab-mattermost/>`__,
`automatic
SSL <https://github.com/kubernetes/charts/tree/master/stable/kube-lego>`__,
and a `load
balancer <https://github.com/kubernetes/ingress/tree/master/controllers/nginx>`__.
It is based on our `GitLab Omnibus Docker
Images <https://docs.gitlab.com/omnibus/docker/README.html>`__.

Once the `cloud native GitLab chart <#cloud-native-gitlab-chart>`__ is
ready for production use, this chart will be deprecated. Due to the
difficulty in supporting upgrades to the new architecture, migrating
will require exporting data out of this instance and importing it into
the new deployment.

Learn more about the `gitlab-omnibus chart <gitlab_omnibus.md>`__.

Cloud Native GitLab Chart
-------------------------

GitLab is working towards building a `cloud native GitLab
chart <https://gitlab.com/charts/helm.gitlab.io/blob/master/README.md>`__.
A key part of this effort is to isolate each service into its `own
Docker container and Helm
chart <https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2420>`__,
rather than utilizing the all-in-one container image of the `current
chart <#gitlab-omnibus-chart-recommended>`__.

By offering individual containers and charts, we will be able to provide
a number of benefits: \* Easier horizontal scaling of each service, \*
Smaller, more efficient images, \* Potential for rolling updates and
canaries within a service, \* and plenty more.

This is a large project and will be worked on over the span of multiple
releases. For the most up-to-date status and release information, please
see our `tracking
issue <https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2420>`__. We
are planning to launch this chart in beta by the end of 2017.

Learn more about the `cloud native GitLab
chart <https://gitlab.com/charts/helm.gitlab.io/blob/master/README.md>`__.

Other Charts
------------

GitLab Runner Chart
~~~~~~~~~~~~~~~~~~~

If you already have a GitLab instance running, inside or outside of
Kubernetes, and you'd like to leverage the Runner's `Kubernetes
capabilities <https://docs.gitlab.com/runner/executors/kubernetes.html>`__,
it can be deployed with the GitLab Runner chart.

Learn more about `gitlab-runner chart. <gitlab_runner_chart.md>`__

Advanced GitLab Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If advanced configuration of GitLab is required, the beta
`gitlab <gitlab_chart.md>`__ chart can be used which deploys the core
GitLab service along with optional Postgres and Redis. It offers
extensive configuration, but offers limited functionality
out-of-the-box; it's lacking Pages support, the container registry, and
Mattermost. It requires deep knowledge of Kubernetes and Helm to use.

This chart will be deprecated and replaced by the
`gitlab-omnibus <gitlab_omnibus.md>`__ chart, once it supports
`additional configuration
options <https://gitlab.com/charts/charts.gitlab.io/issues/68>`__. It's
beta quality, and since it is not actively under development, it will
never be GA.

Learn more about the `gitlab chart. <gitlab_chart.md>`__

Community Contributed Charts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The community has also contributed GitLab
`CE <https://github.com/kubernetes/charts/tree/master/stable/gitlab-ce>`__
and
`EE <https://github.com/kubernetes/charts/tree/master/stable/gitlab-ee>`__
charts to the `Helm Stable
Repository <https://github.com/kubernetes/charts#repository-structure>`__.
These charts should be considered
`deprecated <https://github.com/kubernetes/charts/issues/1138>`__ in
favor of the `official Charts <gitlab_omnibus.md>`__.
